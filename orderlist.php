<?php require('header.php'); ?>
<div class="page-heading">
    <h3>รายการสั่งซื้อ</h3>
</div>
<div class="page-content">
    <div id="loadorderlist"></div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">ข้อมูลการสั่งซื้อ</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="detail_order"></div>
            </div>
            <div class="modal-footer" id="showbutton">
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        loadorderlist();
    });

    function loadorderlist() {
        var loadorderlist = '';

        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/order/get",
            dataType: "json",
            async: false,
            success: function(data) {
                $.each(data.data, function(key, val) {
                    loadorderlist += '<div class="row mb-2 mt-5">';
                    loadorderlist += '<div class="col-md-3 mb-2">วันที่ ' + val.order_date + '</div>';
                    loadorderlist += '<div class="col-md-3">รหัสการสั่งซื้อ ' + val.order_id + '</div>';
                    if (val.paid == 'yes') {
                        loadorderlist += '<div class="col-md-2 mb-2"><a class="btn btn-info btn-sm" onclick="detail_order(' + val.order_id + ')">รายละเอียด</a></div>';
                        loadorderlist += '<div class="col-md-2 mb-2"><i class="bi bi-check-circle-fill text-success"></i> การชำระเงิน</div>';
                    } else {
                        loadorderlist += '<div class="col-md-2 mb-2"></div>';
                        loadorderlist += '<div class="col-md-2 mb-2"><i class="bi bi-x-circle-fill text-danger"></i> การชำระเงิน</div>';
                    }
                    if (val.delivery == 'yes') {
                        loadorderlist += '<div class="col-md-2 mb-2"><i class="bi bi-check-circle-fill text-success"></i> การจัดส่งสินค้า</div>';
                    } else {
                        loadorderlist += '<div class="col-md-2 mb-2"><i class="bi bi-x-circle-fill text-danger"></i> การจัดส่งสินค้า</div>';
                    }
                    loadorderlist += '</div>';
                    loadorderlist += '<div class="table-responsive">';
                    loadorderlist += '<table class="table table-dark mb-0">';
                    loadorderlist += '<thead>';
                    loadorderlist += '<tr>';
                    loadorderlist += '<th class="text-center">ชื่อสินค้า</th>';
                    loadorderlist += '<th class="text-center">คุุณลักษณะ</th>';
                    loadorderlist += '<th class="text-center">จำนวน</th>';
                    loadorderlist += '<th class="text-center">ราคา</th>';
                    loadorderlist += '<th class="text-center">รวม</th>';
                    loadorderlist += '<th class="text-center">ลบ</th>';
                    loadorderlist += '</tr>';
                    loadorderlist += '</thead>';
                    loadorderlist += '<tbody>';
                    var sum_price_all = 0;
                    $.each(val.order_details, function(key_order_details, val_order_details) {
                        var price_qty = val_order_details.quantity * val_order_details.price;
                        sum_price_all = sum_price_all + price_qty;
                        loadorderlist += '<tr>';
                        loadorderlist += '<td class="text-center">' + val_order_details.product_name + '</td>';
                        loadorderlist += '<td class="text-center">' + val_order_details.attribute + '</td>';
                        loadorderlist += '<td class="text-center">' + val_order_details.quantity + '</td>';
                        loadorderlist += '<td class="text-center">' + new Intl.NumberFormat().format(val_order_details.price) + '</td>';
                        loadorderlist += '<td class="text-center">' + new Intl.NumberFormat().format(price_qty) + '</td>';
                        if (val.paid == 'yes') {
                            loadorderlist += '<td></td>';
                        } else {
                            loadorderlist += '<td class="text-center"><a class="btn btn-danger" onclick="dellist(' + val_order_details.item_id + ',' + val.order_id + ')">ลบ</a></td>';
                        }
                        loadorderlist += '</tr>';
                    })
                    loadorderlist += '<tr>';
                    loadorderlist += '<td colspan="4" class="text-center"><b>รวมทั้งหมด</b></td>';
                    loadorderlist += '<td class="text-center"><b>' + new Intl.NumberFormat().format(sum_price_all) + '</b></td>';
                    loadorderlist += '<td></td>';
                    loadorderlist += '</tr>';
                    loadorderlist += '</tbody>';
                    loadorderlist += '</table>';
                    loadorderlist += '</div>';
                })
                $('#loadorderlist').html(loadorderlist);
            },
            error: function(err) {
                loadorderlist += '<tr><td colspan="6" class="text-center">ไม่มีข้อมูลในขณะนี้</td></tr>';
                $('#loadorderlist').html(loadorderlist);
            }
        });
    }

    function dellist(item_id, order_id) {
        if (item_id && order_id) {
            bootbox.confirm({
                message: "คุณต้องการลบรายการนี้ใช่หรือไม่",
                buttons: {
                    confirm: {
                        label: 'ยืนยัน',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'ยกเลิก',
                        className: 'btn-danger'
                    }
                },
                closeButton: false,
                callback: function(result_confirm) {
                    if (result_confirm == true) {
                        $.ajax({
                            method: "POST",
                            url: "<?= $base_url ?>config/order/delete",
                            dataType: "json",
                            data: {
                                item_id: item_id,
                                order_id: order_id
                            },
                            async: false,
                            success: function(data) {
                                loadorderlist();
                                $.notify(data.message, 'success');
                            },
                            error: function(err) {
                                $.notify(err.responseJSON.message, 'error');
                            }
                        });
                    }
                }
            })
        } else {
            $.notify('ไม่มีไอดีที่จะลบ', 'error');
        }
    }

    function detail_order(order_id) {
        var detail_order = '';
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/order/getid",
            dataType: "json",
            async: false,
            data: {
                order_id: order_id
            },
            success: function(data) {
                $.each(data.data, function(key, val) {
                    detail_order += '<table>';
                    detail_order += '<tr>';
                    detail_order += '<td><b>ชื่อลูกค้า: </b></td>';
                    detail_order += '<td>' + val.firstname + ' ' + val.lastname + '</td>';
                    detail_order += '</tr>';
                    detail_order += '<tr>';
                    detail_order += '<td><b>ที่อยู่: </b></td>';
                    detail_order += '<td>' + val.address + '</td>';
                    detail_order += '</tr>';
                    detail_order += '<tr>';
                    detail_order += '<td><b>เบอร์โทร: </b></td>';
                    detail_order += '<td>' + val.phone + '</td>';
                    detail_order += '</tr>';
                    detail_order += '</table>';
                    detail_order += '<div class="table-responsive mt-3">';
                    detail_order += '<table class="table table-dark mb-0">';
                    detail_order += '<thead>';
                    detail_order += '<tr>';
                    detail_order += '<th class="text-center">ชื่อสินค้า</th>';
                    detail_order += '<th class="text-center">คุุณลักษณะ</th>';
                    detail_order += '<th class="text-center">จำนวน</th>';
                    detail_order += '<th class="text-center">ราคา</th>';
                    detail_order += '<th class="text-center">รวม</th>';
                    detail_order += '</tr>';
                    detail_order += '</thead>';
                    detail_order += '<tbody>';
                    var sum_price_all = 0;
                    $.each(val.order_details, function(key_order_details, val_order_details) {
                        var price_qty = val_order_details.quantity * val_order_details.price;
                        sum_price_all = sum_price_all + price_qty;
                        detail_order += '<tr>';
                        detail_order += '<td class="text-center">' + val_order_details.product_name + '</td>';
                        detail_order += '<td class="text-center">' + val_order_details.attribute + '</td>';
                        detail_order += '<td class="text-center">' + val_order_details.quantity + '</td>';
                        detail_order += '<td class="text-center">' + new Intl.NumberFormat().format(val_order_details.price) + '</td>';
                        detail_order += '<td class="text-center">' + new Intl.NumberFormat().format(price_qty) + '</td>';
                        detail_order += '</tr>';
                    })
                    detail_order += '<tr>';
                    detail_order += '<td colspan="3" class="text-center"><b>รวมทั้งหมด</b></td>';
                    detail_order += '<td colspan="2" class="text-center"><b>' + new Intl.NumberFormat().format(sum_price_all) + '</b></td>';
                    detail_order += '</tr>';
                    detail_order += '</tbody>';
                    detail_order += '</table>';
                    detail_order += '</div>';
                    var showbutton = '';
                    if (val.delivery == 'no') {
                        showbutton += '<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ยกเลิก</button>';
                        showbutton += '<button type="button" class="btn btn-primary" onclick="confirmdelivery(' + val.order_id + ');">จัดส่งสินค้า</button>';
                        $('#showbutton').html(showbutton);
                    } else {
                        $('#showbutton').html(showbutton);
                    }
                })

                $('#detail_order').html(detail_order);

                $('#exampleModal').modal('show');
            },
            error: function(err) {
                $.notify(err.responseJSON.message, 'error');
            }
        });
    }

    function confirmdelivery(order_id) {
        if (order_id) {
            $.ajax({
                method: "POST",
                url: "<?= $base_url ?>config/transfer/delivery",
                dataType: "json",
                async: false,
                data: {
                    order_id: order_id
                },
                success: function(data) {
                    loadorderlist();
                    $('#exampleModal').modal('hide');
                    $.notify(data.message, 'success');
                },
                error: function(err) {
                    $.notify(err.responseJSON.message, 'error');
                }
            });
        } else {
            $.notify('ไม่สามารถส่งข้อมูลได้', 'error');
        }
    }
</script>
<?php require('footer.php'); ?>