<?php require('header.php'); ?>
<style>
    img {
        max-width: 300px;
    }
</style>
<div class="page-heading">
    <h3>ข้อมูลลูกค้าที่แจ้งการโอน</h3>
</div>
<div class="page-content">
    <div class="table-responsive">
        <table class="table table-dark mb-0">
            <thead>
                <tr>
                    <th class="text-center" nowrap width="20%">รหัสการสั่งซื้อ</th>
                    <th class="text-center" nowrap width="20%">ชื่อลูกค้า</th>
                    <th class="text-center" nowrap>เบอร์โทรศัพท์</th>
                    <th class="text-center" nowrap>จำนวนเงิน</th>
                    <th class="text-center" nowrap width="10%">สลีปการโอน</th>
                    <th class="text-center" nowrap width="10%">ยืนยันการโอน</th>
                </tr>
            </thead>
            <tbody id="loadcust_transfer">
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">หลักฐานการโอน</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="img" class="text-center"></div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        loadcust_transfer();
    });

    function loadcust_transfer() {
        var loadcust_transfer = '';
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/transfer/get",
            dataType: "json",
            async: false,
            success: function(data) {
                $.each(data.data, function(key, val) {
                    loadcust_transfer += '<tr>';
                    loadcust_transfer += '<td class="text-center">' + val.order_id + '</td>';
                    loadcust_transfer += '<td class="text-center">' + val.firstname + ' ' + val.lastname + '</td>';
                    loadcust_transfer += '<td class="text-center">' + val.phone + '</td>';
                    loadcust_transfer += '<td class="text-center">' + new Intl.NumberFormat().format(val.sumprice) + '</td>';
                    loadcust_transfer += '<td width="10%"><button class="btn btn-warning btn-block" onclick="showslip(\'' + val.slip_path + '\');">slip</button></td>';
                    loadcust_transfer += '<td width="10%"><button class="btn btn-primary btn-block" onclick="confirm(' + val.pay_id + ',' + val.order_id + ');">ยืนยัน</button></td>';
                    loadcust_transfer += '</tr>';
                })
                $('#loadcust_transfer').html(loadcust_transfer);
            },
            error: function(err) {
                loadcust_transfer += '<tr><td colspan="5" class="text-center">ไม่มีข้อมูลในขณะนี้</td></tr>';
                $('#loadcust_transfer').html(loadcust_transfer);
            }
        });
    }

    function showslip(slip_path) {
        console.log(slip_path);
        $('#img').html('<img src="' + slip_path + '" alt="picture" />');
        $('#exampleModal').modal('show');
    }

    function confirm(pay_id, order_id) {
        if (pay_id && order_id) {
            $.ajax({
                method: "POST",
                url: "<?= $base_url ?>config/transfer/confirm",
                dataType: "json",
                async: false,
                data: {
                    pay_id: pay_id,
                    order_id: order_id
                },
                success: function(data) {
                    loadcust_transfer();
                    $.notify(data.message, 'success');
                },
                error: function(err) {
                    $.notify(err.responseJSON.message, 'error');
                }
            });
        } else {
            $.notify('ไม่สามารถส่งข้อมูลได้', 'error');
        }
    }
</script>
<?php require('footer.php'); ?>