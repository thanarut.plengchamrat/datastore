<?php require('header.php'); ?>
<style>
    img {
        max-height: 100px;
    }

    .dataTable-table td,
    .dataTable-table thead th,
    .table td,
    .table thead th {
        vertical-align: top;
    }

    td {
        height: 50px;
        vertical-align: top;
    }
</style>
<div class="page-heading">
    <h3>รายการสินค้า</h3>
</div>
<div class="page-content">
    <button class="btn btn-success mb-2" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="clear_form();">เพิ่มสินค้าใหม่</button>
    <div class="table-responsive">
        <table class="table table-dark mb-0">
            <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">สินค้า</th>
                    <th class="text-center">รายละเอียด</th>
                    <th class="text-center" colspan="2" width="20%">แก้ไข/ลบ</th>
                </tr>
            </thead>
            <tbody id="loadproduct">
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">เพิ่มสินค้าใหม่</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group mb-3">
                    <input type="hidden" id="product_id" class="form-control" name="product_id" placeholder="รหัสสินค้า">
                    <input type="text" id="product_name" class="form-control" name="product_name" placeholder="ชื่อสินค้า">
                </div>
                <div class="form-group mb-3">
                    <textarea class="form-control" id="product_detail" rows="3" placeholder="รายละเอียดสินค้า"></textarea>
                </div>

                <div class="row">
                    <div class="col-sm-6 mb-1">
                        <div class="form-group mb-3">
                            <select class="choices form-select" id="loadcategory">
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 mb-1">
                        <div class="form-group mb-3">
                            <select class="choices form-select" id="loadshipper">
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 mb-1">
                        <div class="input-group mb-3">
                            <input type="text" id="product_price" class="form-control" name="product_price" placeholder="ราคาต่อหน่วย">
                        </div>
                    </div>
                    <div class="col-sm-6 mb-1">
                        <div class="input-group mb-3">
                            <input type="text" id="product_qty" class="form-control" name="product_qty" placeholder="จำนวนสินค้า">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4 mb-1">
                        <div class="input-group mb-3">
                            <input type="hidden" id="attr_id1" class="form-control" name="attr_id1" placeholder="รหัสคุณลักษณะ(1)">
                            <input type="text" id="attr_name1" class="form-control" name="attr_name1" placeholder="ชื่อคุณลักษณะ(1)">
                        </div>
                    </div>
                    <div class="col-sm-8 mb-1">
                        <div class="input-group mb-3">
                            <input type="text" id="attr_value1" class="form-control" name="attr_value1" placeholder="ค่าของคุณลักษณะ">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4 mb-1">
                        <div class="input-group mb-3">
                            <input type="hidden" id="attr_id2" class="form-control" name="attr_id2" placeholder="รหัสคุณลักษณะ(2)">
                            <input type="text" id="attr_name2" class="form-control" name="attr_name2" placeholder="ชื่อคุณลักษณะ(2)">
                        </div>
                    </div>
                    <div class="col-sm-8 mb-1">
                        <div class="input-group mb-3">
                            <input type="text" id="attr_value2" class="form-control" name="attr_value2" placeholder="ค่าของคุณลักษณะ">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4 mb-1">
                        <div class="input-group mb-3">
                            <input type="hidden" id="attr_id3" class="form-control" name="attr_id3" placeholder="รหัสคุณลักษณะ(3)">
                            <input type="text" id="attr_name3" class="form-control" name="attr_name3" placeholder="ชื่อคุณลักษณะ(3)">
                        </div>
                    </div>
                    <div class="col-sm-8 mb-1">
                        <div class="input-group mb-3">
                            <input type="text" id="attr_value3" class="form-control" name="attr_value3" placeholder="ค่าของคุณลักษณะ">
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <input class="form-control" type="file" id="image_uploads">
                    <input class="form-control" type="hidden" id="image_uploads_old">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-primary" onclick="saveproduct();">บันทึก</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        loadcategory();
        loadshipper();
        loadproduct();
    });

    function clear_form() {
        $('#product_id').val('');
        $('#product_name').val('');
        $('#product_detail').val('');
        $('#loadcategory').val('');
        $('#loadshipper').val('');
        $('#product_price').val('');
        $('#product_qty').val('');
        $('#attr_id1').val('');
        $('#attr_id2').val('');
        $('#attr_id3').val('');
        $('#attr_name1').val('');
        $('#attr_name2').val('');
        $('#attr_name3').val('');
        $('#attr_value1').val('');
        $('#attr_value2').val('');
        $('#attr_value3').val('');
        $('#image_uploads').val('');
        $('#image_uploads_old').val('');
    }

    function loadcategory() {
        var text_category = '';
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/category/get",
            dataType: "json",
            async: false,
            success: function(data) {
                $.each(data.data, function(key, val) {
                    text_category += ' <option value="' + val.category_id + '">' + val.category_name + '</option>';
                })
                $('#loadcategory').html(text_category);
            },
            error: function(err) {
                $.notify(err.responseJSON.message, 'error');
            }
        });
    }

    function loadshipper() {
        var text_shipper = '';
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/shipper/get",
            dataType: "json",
            async: false,
            success: function(data) {
                $.each(data.data, function(key, val) {
                    text_shipper += ' <option value="' + val.sup_id + '">' + val.sup_name + '</option>';
                })
                $('#loadshipper').html(text_shipper);
            },
            error: function(err) {
                $.notify(err.responseJSON.message, 'error');
            }
        });
    }

    function loadproduct() {
        var text_product = '';
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/product/get",
            dataType: "json",
            async: false,
            success: function(data) {
                $.each(data.data, function(key, val) {
                    var run = key + 1;
                    text_product += '<tr>';
                    text_product += '<td class="text-center">' + run + '</td>';
                    text_product += '<td><b>ชื่อสินค้า : </b>' + val.product_name + '<br> <b>ราคา : </b>' + val.price + '<br> <b>จำนวน : </b>' + val.quantity + '<br> <b>รูปภาพ : </b><br><img id="' + val.product_id + '" src="' + val.path + '" alt="picture" /></td>';
                    text_product += '<td><b>รายละเอียด : </b>' + val.product_detail + '<br><b>หมวดหมู่ : </b>' + val.category_name + '<br><b>ผู้จัดส่ง : </b>' + val.sup_name + '<br><b>คุณลักษณะ : </b><br>';
                    $.each(val.attr, function(key_attr, valattr) {
                        text_product += '- ' + valattr.attr_name + ' ' + valattr.attr_value + '<br>';
                    });

                    text_product += '</td>';
                    text_product += '<td width="10%"><button class="btn btn-primary btn-block" onclick="list_by_id(' + val.product_id + ');">แก้ไข</button></td><td width="10%"><button class="btn btn-danger btn-block" onclick="delproduct(' + val.product_id + ')">ลบ</button></td>';
                    text_product += '</tr>';
                });
                $('#loadproduct').html(text_product);
            },
            error: function(err) {
                text_product += '<tr><td colspan="4" class="text-center">ไม่มีข้อมูลในขณะนี้</td></tr>';
                $('#loadproduct').html(text_product);
            }
        });
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#picture').html("<img id='blah' src='' alt='picture' />");
                $('#blah')
                    .attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    function saveproduct() {
        var product_id = $('#product_id').val();
        var product_name = $('#product_name').val();
        var product_detail = $('#product_detail').val();
        var loadcategory = $('#loadcategory').val();
        var loadshipper = $('#loadshipper').val();
        var product_price = $('#product_price').val();
        var product_qty = $('#product_qty').val();
        var attr_id1 = $('#attr_id1').val();
        var attr_id2 = $('#attr_id2').val();
        var attr_id3 = $('#attr_id3').val();
        var attr_name1 = $('#attr_name1').val();
        var attr_name2 = $('#attr_name2').val();
        var attr_name3 = $('#attr_name3').val();
        var attr_value1 = $('#attr_value1').val();
        var attr_value2 = $('#attr_value2').val();
        var attr_value3 = $('#attr_value3').val();
        var image_uploads = $('#image_uploads').val();
        var image_uploads_old = $('#image_uploads_old').val();

        if (product_id) {
            if (!product_name || !product_detail || !loadcategory || !loadshipper || !product_price ||
                !product_qty) {
                $.notify("โปรดกรอกข้อมูลให้ครบถ้วน", 'error');
            } else {
                if ((!attr_name1 && attr_value1) || (!attr_name2 && attr_value2) || (!attr_name3 && attr_value3) || (attr_name1 && !attr_value1) || (attr_name2 && !attr_value2) || (attr_name3 && !attr_value3)) {
                    $.notify("โปรดกรอกในช่องที่ตรงกัน", 'error');
                } else {
                    if ((!attr_name1 && !attr_value1) && (!attr_name2 && !attr_value2) && (!attr_name3 && !attr_value3)) {
                        $.notify("โปรดกรอกอย่างน้อย 1 คุณลักษณะ", 'error');
                    } else {
                        bootbox.confirm({
                            message: "คุณต้องการเปลี่ยนแปลงข้อมูลใช่หรือไม่",
                            buttons: {
                                confirm: {
                                    label: 'ยืนยัน',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'ยกเลิก',
                                    className: 'btn-danger'
                                }
                            },
                            closeButton: false,
                            callback: function(result_confirm) {
                                if (result_confirm == true) {
                                    $.ajax({
                                        method: "POST",
                                        url: "<?= $base_url ?>config/product/update",
                                        dataType: "json",
                                        async: false,
                                        data: {
                                            product_name: product_name,
                                            product_detail: product_detail,
                                            category_id: loadcategory,
                                            sup_id: loadshipper,
                                            price: product_price,
                                            quantity: product_qty,
                                            product_id: product_id,
                                        },
                                        success: function(result) {
                                            $.notify(result.message, 'success');
                                            var filelength = document.getElementById('image_uploads').files.length;
                                            if (!image_uploads_old) {
                                                if (filelength > 0) {
                                                    var fileInput = document.getElementById('image_uploads').files[0];
                                                    var fileName = fileInput.name;
                                                    var fileSize = fileInput.size;
                                                    var fileType = fileInput.type;
                                                    var fileModifiedDate = fileInput.lastModifiedDate;
                                                    // ------------------------------------------------------------------
                                                    if (fileSize > 25000000) {
                                                        var file_max = "ไฟล์ " + fileName + " ของคุณมีขนาดเกิน 25 MB";
                                                        alert(file_max);
                                                        $('#image_uploads').val('');
                                                    } else {
                                                        if (fileType === 'image/jpeg' || fileType === 'image/jpg' || fileType === 'image/png' || fileType === 'image/gif') {
                                                            var formData = new FormData();
                                                            formData.append('file', fileInput);
                                                            $.ajax({
                                                                type: 'POST',
                                                                url: '<?= $base_url ?>config/product/upload/add?product_id=' + product_id,
                                                                dataType: "json",
                                                                contentType: false,
                                                                processData: false,
                                                                data: formData,
                                                                success: function(response) {
                                                                    $.notify(response.message, 'success');
                                                                },
                                                                error: function(err) {
                                                                    $.notify(err.responseJSON.message, 'error');
                                                                }
                                                            });

                                                        }
                                                    }
                                                } else {
                                                    $.notify("โปรดเพิ่มรูปในภายหลัง", 'info');
                                                }
                                            } else {
                                                if (filelength > 0) {
                                                    var fileInput = document.getElementById('image_uploads').files[0];
                                                    var fileName = fileInput.name;
                                                    var fileSize = fileInput.size;
                                                    var fileType = fileInput.type;
                                                    var fileModifiedDate = fileInput.lastModifiedDate;
                                                    // ------------------------------------------------------------------
                                                    if (fileSize > 25000000) {
                                                        var file_max = "ไฟล์ " + fileName + " ของคุณมีขนาดเกิน 25 MB";
                                                        alert(file_max);
                                                        $('#image_uploads').val('');
                                                    } else {
                                                        if (fileType === 'image/jpeg' || fileType === 'image/jpg' || fileType === 'image/png' || fileType === 'image/gif') {
                                                            var formData = new FormData();
                                                            formData.append('file', fileInput);
                                                            $.ajax({
                                                                type: 'POST',
                                                                url: '<?= $base_url ?>config/product/upload/update?product_id=' + product_id,
                                                                dataType: "json",
                                                                contentType: false,
                                                                processData: false,
                                                                data: formData,
                                                                success: function(response) {
                                                                    $.notify(response.message, 'success');
                                                                },
                                                                error: function(err) {
                                                                    $.notify(err.responseJSON.message, 'error');
                                                                }
                                                            });
                                                        }
                                                    }
                                                }
                                            }

                                            $.ajax({
                                                method: "POST",
                                                url: "<?= $base_url ?>config/product/attributes/update",
                                                dataType: "json",
                                                async: false,
                                                data: {
                                                    attr_id1: attr_id1,
                                                    attr_id2: attr_id2,
                                                    attr_id3: attr_id3,
                                                    attr_name1: attr_name1,
                                                    attr_name2: attr_name2,
                                                    attr_name3: attr_name3,
                                                    attr_value1: attr_value1,
                                                    attr_value2: attr_value2,
                                                    attr_value3: attr_value3,
                                                    product_id: product_id,
                                                },
                                                success: function(result) {
                                                    setTimeout(function() {
                                                        loadproduct();
                                                        $('#exampleModal').modal('hide');
                                                    }, 3000)
                                                    $.notify(result.message, 'success');
                                                },
                                                error: function(err) {
                                                    $.notify(err.responseJSON.message, 'error');
                                                }
                                            })
                                        },
                                        error: function(err) {
                                            $.notify(result.message, 'error');
                                        }
                                    });
                                }
                            }
                        })
                    }
                }
            }
        } else {
            if (!product_name || !product_detail || !loadcategory || !loadshipper || !product_price ||
                !product_qty) {
                $.notify("โปรดกรอกข้อมูลให้ครบถ้วน", 'error');
            } else {
                if ((!attr_name1 && attr_value1) || (!attr_name2 && attr_value2) || (!attr_name3 && attr_value3) || (attr_name1 && !attr_value1) || (attr_name2 && !attr_value2) || (attr_name3 && !attr_value3)) {
                    $.notify("โปรดกรอกในช่องที่ตรงกัน", 'error');
                } else {
                    if ((!attr_name1 && !attr_value1) && (!attr_name2 && !attr_value2) && (!attr_name3 && !attr_value3)) {
                        $.notify("โปรดกรอกอย่างน้อย 1 คุณลักษณะ", 'error');
                    } else {
                        $.ajax({
                            method: "POST",
                            url: "<?= $base_url ?>config/product/insert",
                            dataType: "json",
                            async: false,
                            data: {
                                product_name: product_name,
                                product_detail: product_detail,
                                category_id: loadcategory,
                                sup_id: loadshipper,
                                price: product_price,
                                quantity: product_qty,
                            },
                            success: function(result) {
                                $.notify(result.message, 'success');
                                var filelength = document.getElementById('image_uploads').files.length;
                                if (filelength > 0) {
                                    var fileInput = document.getElementById('image_uploads').files[0];
                                    var fileName = fileInput.name;
                                    var fileSize = fileInput.size;
                                    var fileType = fileInput.type;
                                    var fileModifiedDate = fileInput.lastModifiedDate;
                                    // ------------------------------------------------------------------
                                    if (fileSize > 25000000) {
                                        var file_max = "ไฟล์ " + fileName + " ของคุณมีขนาดเกิน 25 MB";
                                        alert(file_max);
                                        $('#image_uploads').val('');
                                    } else {
                                        if (fileType === 'image/jpeg' || fileType === 'image/jpg' || fileType === 'image/png' || fileType === 'image/gif') {
                                            var formData = new FormData();
                                            formData.append('file', fileInput);
                                            $.ajax({
                                                type: 'POST',
                                                url: '<?= $base_url ?>config/product/upload/add?product_id=' + result.product_id,
                                                dataType: "json",
                                                contentType: false,
                                                processData: false,
                                                data: formData,
                                                success: function(response) {
                                                    $.notify(response.message, 'success');
                                                },
                                                error: function(err) {
                                                    $.notify(err.responseJSON.message, 'error');
                                                }
                                            });
                                        }
                                    }
                                } else {
                                    $.notify("โปรดเพิ่มรูปในภายหลัง", 'info');
                                }
                                $.ajax({
                                    method: "POST",
                                    url: "<?= $base_url ?>config/product/attributes/insert",
                                    dataType: "json",
                                    async: false,
                                    data: {
                                        attr_name1: attr_name1,
                                        attr_name2: attr_name2,
                                        attr_name3: attr_name3,
                                        attr_value1: attr_value1,
                                        attr_value2: attr_value2,
                                        attr_value3: attr_value3,
                                        product_id: result.product_id,
                                    },
                                    success: function(result) {
                                        setTimeout(function() {
                                            loadproduct();
                                            $('#exampleModal').modal('hide');
                                        }, 3000)
                                        $.notify(result.message, 'success');
                                    },
                                    error: function(err) {
                                        $.notify(err.responseJSON.message, 'error');
                                    }
                                })
                            },
                            error: function(err) {
                                $.notify(err.responseJSON.message, 'error');
                            }
                        });
                    }
                }
            }
        }
    }

    function delproduct(product_id) {
        bootbox.confirm({
            message: "คุณต้องการลบข้อมูลสินค้าใช่หรือไม่",
            buttons: {
                confirm: {
                    label: 'ยืนยัน',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'ยกเลิก',
                    className: 'btn-danger'
                }
            },
            closeButton: false,
            callback: function(result_confirm) {
                if (result_confirm == true) {
                    $.ajax({
                        method: "POST",
                        url: "<?= $base_url ?>config/product/delete",
                        dataType: "json",
                        data: {
                            product_id: product_id
                        },
                        async: false,
                        success: function(data) {
                            loadproduct();
                            $.notify(data.message, 'success');
                        },
                        error: function(err) {
                            $.notify(err.responseJSON.message, 'error');
                        }
                    });
                }
            }
        })

    }

    function list_by_id(product_id) {
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/product/getid",
            dataType: "json",
            async: false,
            data: {
                product_id: product_id
            },
            success: function(data) {
                console.log(data);
                $('#product_id').val(data.data.product_id);
                $('#product_name').val(data.data.product_name);
                $('#product_detail').val(data.data.product_detail);
                $('#loadcategory').val(data.data.category_id);
                $('#loadshipper').val(data.data.sup_id);
                $('#product_price').val(data.data.price);
                $('#product_qty').val(data.data.quantity);
                if (data.data.path) {
                    $('#image_uploads_old').val(data.data.path);
                }
                $.each(data.data.attr, function(key, val) {
                    var run = key + 1;
                    $('#attr_id' + run).val(val.attr_id);
                    $('#attr_name' + run).val(val.attr_name);
                    $('#attr_value' + run).val(val.attr_value);
                })

                $('#exampleModal').modal('show');
            },
            error: function(err) {
                $.notify(err.responseJSON.message, 'error');
            }
        });
    }
</script>
<?php require('footer.php'); ?>