<?php require('header.php'); ?>
<div class="page-heading">
    <h3>ผู้จัดส่งสินค้า</h3>
</div>
<div class="page-content">
    <button class="btn btn-success mb-2" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="clear_form();">เพิ่มผู้จัดส่งสินค้า</button>
    <div class="table-responsive">
        <table class="table table-dark mb-0">
            <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">ชื่อผู้จัดส่งสินค้า</th>
                    <th class="text-center">ที่อยู่</th>
                    <th class="text-center">โทรศัพท์</th>
                    <th class="text-center">บุคคลในการติดต่อ</th>
                    <th class="text-center">Website</th>
                    <th class="text-center" width="20%" colspan="2">แก้ไข/ลบ</th>
                </tr>
            </thead>
            <tbody id="load_shipper">
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">เพิ่มผู้จัดส่งสินค้า</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group mb-3">
                    <input type="hidden" id="sup_id" class="form-control" name="sup_id" placeholder="รหัสผู้จัดส่งสินค้า">
                    <input type="text" id="sup_name" class="form-control" name="sup_name" placeholder="ชื่อผู้จัดส่งสินค้า">
                </div>
                <div class="form-group mb-3">
                    <textarea class="form-control" id="address" rows="3" placeholder="ที่อยู่"></textarea>
                </div>
                <div class="form-group mb-3">
                    <input type="text" id="phone" class="form-control" name="phone" placeholder="โทร">
                </div>
                <div class="form-group mb-3">
                    <input type="text" id="contact_name" class="form-control" name="contact_name" placeholder="บุคคลในการติดต่อ">
                </div>
                <div class="form-group mb-3">
                    <input type="text" id="website" class="form-control" name="website" placeholder="เว็บไซต์">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-primary" onclick="saveshipper()">บันทึก</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        loadshipper();
    });

    function clear_form() {
        $('#sup_id').val('');
        $('#sup_name').val('');
        $('#address').val('');
        $('#phone').val('');
        $('#contact_name').val('');
        $('#website').val('');
    }

    function loadshipper() {
        var text_shipper = '';
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/shipper/get",
            dataType: "json",
            async: false,
            success: function(data) {
                $.each(data.data, function(key, val) {
                    var run = key + 1;
                    text_shipper += '<tr>';
                    text_shipper += '<td class="text-center">' + run + '</td>';
                    text_shipper += '<td>' + val.sup_name + '</td>';
                    text_shipper += '<td>' + val.address + '</td>';
                    text_shipper += '<td>' + val.phone + '</td>';
                    text_shipper += '<td>' + val.contact_name + '</td>';
                    text_shipper += '<td>' + val.website + '</td>';
                    text_shipper += '<td width="10%"><button class="btn btn-primary btn-block" onclick="list_by_id(' + val.sup_id + ');">แก้ไข</button></td><td width="10%"><button class="btn btn-danger btn-block" onclick="delshipper(' + val.sup_id + ')">ลบ</button></td>';
                    text_shipper += '</tr>';
                })
                $('#load_shipper').html(text_shipper);
            },
            error: function(err) {
                text_shipper += '<tr><td colspan="4" class="text-center">ไม่มีข้อมูลในขณะนี้</td></tr>';
                $('#load_shipper').html(text_shipper);
                // $.notify(err.responseJSON.message, 'error');
            }
        });
    }

    function saveshipper() {
        var sup_id = $('#sup_id').val();
        var sup_name = $('#sup_name').val();
        var address = $('#address').val();
        var phone = $('#phone').val();
        var contact_name = $('#contact_name').val();
        var website = $('#website').val();

        if (sup_id) {
            if (sup_name && address && phone && contact_name) {
                $.ajax({
                    method: "POST",
                    url: "<?= $base_url ?>config/shipper/update",
                    dataType: "json",
                    async: false,
                    data: {
                        sup_id: sup_id,
                        sup_name: sup_name,
                        address: address,
                        phone: phone,
                        contact_name: contact_name,
                        website: website
                    },
                    success: function(data) {
                        $('#exampleModal').modal('hide');
                        loadshipper();
                        $.notify(data.message, 'success');
                    },
                    error: function(err) {
                        $.notify(err.responseJSON.message, 'error');
                    }
                });
            } else {
                $.notify('โปรดกรอกข้อมูลผู้จัดส่งสินค้า', 'error');
            }
        } else {
            if (sup_name && address && phone && contact_name) {
                $.ajax({
                    method: "POST",
                    url: "<?= $base_url ?>config/shipper/insert",
                    dataType: "json",
                    async: false,
                    data: {
                        sup_name: sup_name,
                        address: address,
                        phone: phone,
                        contact_name: contact_name,
                        website: website
                    },
                    success: function(data) {
                        $('#exampleModal').modal('hide');
                        loadshipper();
                        $.notify(data.message, 'success');
                    },
                    error: function(err) {
                        $.notify(err.responseJSON.message, 'error');
                    }
                });
            } else {
                $.notify('โปรดกรอกข้อมูลผู้จัดส่งสินค้า', 'error');
            }
        }
    }

    function delshipper(sup_id) {
        bootbox.confirm({
            message: "คุณต้องการลบข้อมูลผู้จัดส่งสินค้าใช่หรือไม่",
            buttons: {
                confirm: {
                    label: 'ยืนยัน',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'ยกเลิก',
                    className: 'btn-danger'
                }
            },
            closeButton: false,
            callback: function(result_confirm) {
                if (result_confirm == true) {
                    $.ajax({
                        method: "POST",
                        url: "<?= $base_url ?>config/shipper/delete",
                        dataType: "json",
                        data: {
                            sup_id: sup_id
                        },
                        async: false,
                        success: function(data) {
                            loadcategory();
                            $.notify(data.message, 'success');
                        },
                        error: function(err) {
                            $.notify(err.responseJSON.message, 'error');
                        }
                    });
                }
            }
        })

    }

    function list_by_id(sup_id) {
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/shipper/getid",
            dataType: "json",
            async: false,
            data: {
                sup_id: sup_id
            },
            success: function(data) {
                $('#sup_id').val(data.sup_id);
                $('#sup_name').val(data.sup_name);
                $('#address').val(data.address);
                $('#phone').val(data.phone);
                $('#contact_name').val(data.contact_name);
                $('#website').val(data.website);
                $('#exampleModal').modal('show');
            },
            error: function(err) {
                $.notify(err.responseJSON.message, 'error');
            }
        });
    }
</script>
<?php require('footer.php'); ?>