<?php require('header.php'); ?>
<div class="page-heading">
    <h3>ข้อมูลลูกค้า</h3>
</div>
<div class="page-content">
    <div class="table-responsive">
        <table class="table table-dark mb-0">
            <thead>
                <tr>
                    <th class="text-center" width="20%">ชื่อลูกค้า</th>
                    <th class="text-center">เบอร์โทรศัพท์</th>
                    <th class="text-center">ที่อยู่ในการส่ง</th>
                    <th class="text-center" width="10%">แก้ไข</th>
                </tr>
            </thead>
            <tbody id="loadcustomer">
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">ช้อมูลลูกค้า</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group mb-3">
                    <input type="hidden" id="cust_id" class="form-control" name="cust_id" placeholder="รหัสลูกค้า">
                    <input type="text" id="firstname" class="form-control" name="firstname" placeholder="ชื่อ">
                </div>
                <div class="form-group mb-3">
                    <input type="text" id="lastname" class="form-control" name="lastname" placeholder="นามสกุล">
                </div>
                <div class="form-group mb-3">
                    <input type="text" id="phone" class="form-control" name="phone" placeholder="เบอร์โทรศัพท์">
                </div>
                <div class="form-group mb-3">
                    <textarea class="form-control" id="address" rows="5" aria-label="ที่อยู่" placeholder="ที่อยู่"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-primary" onclick="savecustomer();">บันทึก</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        loadcustomer();
    });

    function loadcustomer() {
        var loadcustomer = '';
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/customer/get",
            dataType: "json",
            async: false,
            success: function(data) {
                $.each(data.data, function(key, val) {
                    loadcustomer += '<tr>';
                    loadcustomer += '<td class="text-center">' + val.firstname + ' ' + val.lastname + '</td>';
                    loadcustomer += '<td class="text-center">' + val.phone + '</td>';
                    loadcustomer += '<td class="text-center">' + val.address + '</td>';
                    loadcustomer += '<td width="10%"><button class="btn btn-primary btn-block" onclick="list_by_id(' + val.cust_id + ');">แก้ไข</button></td>';
                    loadcustomer += '</tr>';
                })
                $('#loadcustomer').html(loadcustomer);
            },
            error: function(err) {
                loadcustomer += '<tr><td colspan="4" class="text-center">ไม่มีข้อมูลในขณะนี้</td></tr>';
                $('#loadcustomer').html(loadcustomer);
            }
        });
    }

    function savecustomer() {
        var firstname = $('#firstname').val();
        var lastname = $('#lastname').val();
        var address = $('#address').val();
        var phone = $('#phone').val();
        var cust_id = $('#cust_id').val();
        if (cust_id) {
            if (firstname && lastname && address && phone) {
                $.ajax({
                    method: "POST",
                    url: "<?= $base_url ?>config/customer/update",
                    dataType: "json",
                    async: false,
                    data: {
                        firstname: firstname,
                        lastname: lastname,
                        address: address,
                        phone: phone,
                        cust_id: cust_id
                    },
                    success: function(data) {
                        $('#exampleModal').modal('hide');
                        loadcustomer();
                        $.notify(data.message, 'success');
                    },
                    error: function(err) {
                        $.notify(err.responseJSON.message, 'error');
                    }
                });
            } else {
                $.notify('โปรดกรอกข้อมูลลูกค้า', 'error');
            }
        } else {
            $.notify('ไม่เจอข้อมูลที่ต้องการเปลี่ยน', 'error');
        }
    }


    function list_by_id(cust_id) {
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/customer/getid",
            dataType: "json",
            async: false,
            data: {
                cust_id: cust_id
            },
            success: function(result) {
                $('#cust_id').val(result.data.cust_id);
                $('#firstname').val(result.data.firstname);
                $('#lastname').val(result.data.lastname);
                $('#address').val(result.data.address);
                $('#phone').val(result.data.phone);

                $('#exampleModal').modal('show');
            },
            error: function(err) {
                $.notify(err.responseJSON.message, 'error');
            }
        });
    }
</script>
<?php require('footer.php'); ?>