<?php
if ($_SERVER['HTTPS']) {
    $base_url = 'https://localhost/datastore/';
} else {
    $new_url = (empty($_SERVER['HTTPS']) ? 'https' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    header("location: " . $new_url);
    exit(0);
    // $base_url = 'http://localhost/datastore/';
}
session_start();
if ($_SESSION['username'] != "test@gmail.com") {
    header('location: login');
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Store</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= $base_url ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= $base_url ?>assets/vendors/iconly/bold.css">
    <link rel="stylesheet" href="<?= $base_url ?>assets/vendors/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?= $base_url ?>assets/vendors/bootstrap-icons/bootstrap-icons.css">
    <link rel="stylesheet" href="<?= $base_url ?>assets/css/app.css">
    <link rel="stylesheet" href="<?= $base_url ?>assets/vendors/fontawesome/all.min.css">
    <link rel="shortcut icon" href="<?= $base_url ?>assets/images/favicon.svg" type="image/x-icon">

    <script src="<?= $base_url ?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?= $base_url ?>assets/js/jquery.min.js"></script>

</head>

<body>
    <div id="app">
        <div id="sidebar" class="active">
            <div class="sidebar-wrapper active">
                <div class="sidebar-header">
                    <div class="d-flex justify-content-between">
                        <div class="logo">
                            <a href="index">
                                <i class="bi bi-basket-fill"></i>
                                จัดการสินค้า
                            </a>
                        </div>
                        <div class="toggler">
                            <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                        </div>
                    </div>
                </div>
                <div class="sidebar-menu">
                    <ul class="menu">
                        <li class="sidebar-title">Menu</li>
                        <?php
                        if (($_SERVER['REQUEST_URI'] == '/datastore/') || ($_SERVER['REQUEST_URI'] == '/datastore/index')) {
                        ?>
                            <li class="sidebar-item active ">
                                <a href="index" class='sidebar-link'>
                                    <i class="bi bi-grid-fill"></i>
                                    <span>รายการสินค้า</span>
                                </a>
                            </li>
                        <?php
                        } else {
                        ?>
                            <li class="sidebar-item">
                                <a href="index" class='sidebar-link'>
                                    <i class="bi bi-grid-fill"></i>
                                    <span>รายการสินค้า</span>
                                </a>
                            </li>
                        <?php
                        }
                        ?>

                        <?php
                        if ($_SERVER['REQUEST_URI'] == '/datastore/category') {
                        ?>
                            <li class="sidebar-item active ">
                                <a href="category" class='sidebar-link'>
                                    <i class="bi bi-grid-fill"></i>
                                    <span>หมวดหมู่สินค้า</span>
                                </a>
                            </li>
                        <?php
                        } else {
                        ?>
                            <li class="sidebar-item">
                                <a href="category" class='sidebar-link'>
                                    <i class="bi bi-grid-fill"></i>
                                    <span>หมวดหมู่สินค้า</span>
                                </a>
                            </li>
                        <?php
                        }
                        ?>

                        <?php
                        if ($_SERVER['REQUEST_URI'] == '/datastore/shipper') {
                        ?>
                            <li class="sidebar-item active ">
                                <a href="shipper" class='sidebar-link'>
                                    <i class="bi bi-grid-fill"></i>
                                    <span>ผู้จัดส่งสินค้า</span>
                                </a>
                            </li>
                        <?php
                        } else {
                        ?>
                            <li class="sidebar-item">
                                <a href="shipper" class='sidebar-link'>
                                    <i class="bi bi-grid-fill"></i>
                                    <span>ผู้จัดส่งสินค้า</span>
                                </a>
                            </li>
                        <?php
                        }
                        ?>

                        <?php
                        if ($_SERVER['REQUEST_URI'] == '/datastore/orderlist') {
                        ?>
                            <li class="sidebar-item active ">
                                <a href="orderlist" class='sidebar-link'>
                                    <i class="bi bi-grid-fill"></i>
                                    <span>รายการสั่งซื้อ</span>
                                </a>
                            </li>
                        <?php
                        } else {
                        ?>
                            <li class="sidebar-item">
                                <a href="orderlist" class='sidebar-link'>
                                    <i class="bi bi-grid-fill"></i>
                                    <span>รายการสั่งซื้อ</span>
                                </a>
                            </li>
                        <?php
                        }
                        ?>

                        <?php
                        if ($_SERVER['REQUEST_URI'] == '/datastore/transfer') {
                        ?>
                            <li class="sidebar-item active ">
                                <a href="transfer" class='sidebar-link'>
                                    <i class="bi bi-grid-fill"></i>
                                    <span>แจ้งการโอนเงิน</span>
                                </a>
                            </li>
                        <?php
                        } else {
                        ?>
                            <li class="sidebar-item">
                                <a href="transfer" class='sidebar-link'>
                                    <i class="bi bi-grid-fill"></i>
                                    <span>แจ้งการโอนเงิน</span>
                                </a>
                            </li>
                        <?php
                        }
                        ?>

                        <?php
                        if ($_SERVER['REQUEST_URI'] == '/datastore/customer') {
                        ?>
                            <li class="sidebar-item active ">
                                <a href="customer" class='sidebar-link'>
                                    <i class="bi bi-grid-fill"></i>
                                    <span>ข้อมูลลูกค้า</span>
                                </a>
                            </li>
                        <?php
                        } else {
                        ?>
                            <li class="sidebar-item">
                                <a href="customer" class='sidebar-link'>
                                    <i class="bi bi-grid-fill"></i>
                                    <span>ข้อมูลลูกค้า</span>
                                </a>
                            </li>
                        <?php
                        }
                        ?>
                        <li class="sidebar-item">
                            <a href="<?= $base_url ?>home?product=all" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>ไปที่ร้านค้า</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?= $base_url ?>config/logout" class='sidebar-link'>
                                <i class="bi bi-power"></i>
                                <span>ออกจากระบบ</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
            </div>
        </div>
        <div id="main">
            <header class="mb-3">
                <a href="#" class="burger-btn d-block d-xl-none">
                    <i class="bi bi-justify fs-3"></i>
                </a>
            </header>