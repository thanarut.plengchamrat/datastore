<?php require('header.php'); ?>
<div class="page-heading">
    <h3>หมวดหมู่สินค้า</h3>
</div>
<div class="page-content">
    <button class="btn btn-success mb-2" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="clear_form();">เพิ่มหมวดหมู่ใหม่</button>
    <div class="table-responsive">
        <table class="table table-dark mb-0">
            <thead>
                <tr>
                    <th class="text-center" width="20%">รหัสหมวดหมู่</th>
                    <th class="text-center" width="60%">ชื่อหมวดหมู่</th>
                    <th class="text-center" width="20%" colspan="2">แก้ไข/ลบ</th>
                </tr>
            </thead>
            <tbody id="loadcategory">
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">เพิ่มหมวดหมู่ใหม่</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group mb-3">
                    <input type="hidden" id="category_id" class="form-control" name="category_id" placeholder="รหัสหมวดหมู่">
                    <input type="text" id="category_name" class="form-control" name="category_name" placeholder="ชื่อหมวดหมู่">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-primary" onclick="savecategory();">บันทึก</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        loadcategory();
    });

    function clear_form() {
        $('#category_id').val('');
        $('#category_name').val('');
    }

    function loadcategory() {
        var text_category = '';
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/category/get",
            dataType: "json",
            async: false,
            success: function(data) {
                $.each(data.data, function(key, val) {
                    text_category += '<tr>';
                    text_category += '<td class="text-center">' + val.category_id + '</td>';
                    text_category += '<td>' + val.category_name + '</td>';
                    text_category += '<td width="10%"><button class="btn btn-primary btn-block" onclick="list_by_id(' + val.category_id + ');">แก้ไข</button></td><td width="10%"><button class="btn btn-danger btn-block" onclick="delcategory(' + val.category_id + ')">ลบ</button></td>';
                    text_category += '</tr>';
                })
                $('#loadcategory').html(text_category);
            },
            error: function(err) {
                text_category += '<tr><td colspan="4" class="text-center">ไม่มีข้อมูลในขณะนี้</td></tr>';
                $('#loadcategory').html(text_category);
                // $.notify(err.responseJSON.message, 'error');
            }
        });
    }

    function savecategory() {
        var category_id = $('#category_id').val();
        var category_name = $('#category_name').val();
        if (category_id) {
            if (category_name) {
                $.ajax({
                    method: "POST",
                    url: "<?= $base_url ?>config/category/update",
                    dataType: "json",
                    async: false,
                    data: {
                        category_id: category_id,
                        category_name: category_name
                    },
                    success: function(data) {
                        $('#exampleModal').modal('hide');
                        loadcategory();
                        $.notify(data.message, 'success');
                    },
                    error: function(err) {
                        $.notify(err.responseJSON.message, 'error');
                    }
                });
            } else {
                $.notify('โปรดกรอกข้อมูลหมวดหมู่', 'error');
            }
        } else {
            if (category_name) {
                $.ajax({
                    method: "POST",
                    url: "<?= $base_url ?>config/category/insert",
                    dataType: "json",
                    async: false,
                    data: {
                        category_name: category_name
                    },
                    success: function(data) {

                        $('#exampleModal').modal('hide');
                        loadcategory();
                        $.notify(data.message, 'success');
                    },
                    error: function(err) {
                        $.notify(err.responseJSON.message, 'error');
                    }
                });
            } else {
                $.notify('โปรดกรอกข้อมูลหมวดหมู่', 'error');
            }
        }
    }

    function delcategory(category_id) {
        bootbox.confirm({
            message: "คุณต้องการลบข้อมูลหมวดหมู่ใช่หรือไม่",
            buttons: {
                confirm: {
                    label: 'ยืนยัน',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'ยกเลิก',
                    className: 'btn-danger'
                }
            },
            closeButton: false,
            callback: function(result_confirm) {
                if (result_confirm == true) {
                    $.ajax({
                        method: "POST",
                        url: "<?= $base_url ?>config/category/delete",
                        dataType: "json",
                        data: {
                            category_id: category_id
                        },
                        async: false,
                        success: function(data) {
                            loadcategory();
                            $.notify(data.message, 'success');
                        },
                        error: function(err) {
                            $.notify(err.responseJSON.message, 'error');
                        }
                    });
                }
            }
        })

    }

    function list_by_id(category_id) {
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/category/getid",
            dataType: "json",
            async: false,
            data: {
                category_id: category_id
            },
            success: function(data) {
                $('#category_id').val(data.category_id);
                $('#category_name').val(data.category_name);
                $('#exampleModal').modal('show');
            },
            error: function(err) {
                $.notify(err.responseJSON.message, 'error');
            }
        });
    }
</script>
<?php require('footer.php'); ?>