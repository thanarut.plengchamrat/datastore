<?php
require_once('../database.php');
if ($_POST['item_id'] && $_POST['order_id']) {
    $item_id = $_POST['item_id'];
    $order_id = $_POST['order_id'];
    $delete_item_id = "DELETE FROM order_details WHERE item_id='$item_id'";
    if (mysqli_query($conn, $delete_item_id)) {
        $sql = " SELECT * FROM `order_details` WHERE order_id = '$order_id'";
        $query = mysqli_query($conn, $sql);
        $result = mysqli_fetch_all($query, MYSQLI_ASSOC);
        if ($result) {
            $data['message'] = "ลบข้อมูลสำเร็จ";
            http_response_code(200);
        } else {
            $delete_order_id = "DELETE FROM `order` WHERE order_id='$order_id'";
            if (mysqli_query($conn, $delete_order_id)) {
                $data['message'] = "ลบข้อมูลสำเร็จ";
                http_response_code(200);
            } else {
                $data['message'] = "ไม่สามารถลบข้อมูลได้";
                http_response_code(400);
            }
        }
    } else {
        $data['message'] = "ไม่สามารถลบข้อมูลได้";
        http_response_code(400);
    }
} else {
    $data['message'] = "การส่งข้อมูลไม่ถูกต้อง";
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
