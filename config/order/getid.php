<?php
require_once('../database.php');
if ($_POST['order_id']) {
    $order_id = $_POST['order_id'];
    $sql = "SELECT * FROM `order` JOIN customer ON customer.cust_id = order.cust_id WHERE `order`.`order_id` = '$order_id' ORDER BY `order`.`paid` DESC";
    $query = mysqli_query($conn, $sql);
    $result = mysqli_fetch_all($query, MYSQLI_ASSOC);
    if ($result) {
        $data['data'] = $result;
        foreach ($result as $key => $item) {
            $sqldetails = "SELECT order_details.*,product.product_name,product.price,product.product_detail FROM order_details 
        JOIN product ON product.product_id = order_details.product_id
        WHERE order_id = '$order_id'";
            $querydetails = mysqli_query($conn, $sqldetails);
            $resultdetails = mysqli_fetch_all($querydetails, MYSQLI_ASSOC);
            if ($resultdetails) {
                $data['data'][$key]['order_details'] = $resultdetails;
                http_response_code(200);
            } else {
                $data['message'] = "ไม่มีข้อมูลการสั่งสินค้า";
                $data['quantity'] = 0;
                http_response_code(400);
            }
        }
        $data['message'] = "ดึงข้อมูลสำเร็จ";
        http_response_code(200);
    } else {
        $data['message'] = "ไม่สามารถดึงข้อมูลได้";
        http_response_code(400);
    }
} else {
    $data['message'] = "ไม่มีไอดีในการค้นหา";
    http_response_code(400);
}
echo json_encode($data, JSON_UNESCAPED_UNICODE);
mysqli_close($conn);
