<?php
require_once('../database.php');
$sql = "SELECT * FROM `payment` JOIN customer ON customer.cust_id = payment.cust_id WHERE confirm = 'no'";
$query = mysqli_query($conn, $sql);
$result = mysqli_fetch_all($query, MYSQLI_ASSOC);
if ($result) {
    $data['data'] = $result;
    foreach ($result as $key => $item) {
        $order_id = $item['order_id'];
        $sqlsum = "SELECT SUM(order_details.quantity * product.price) as sumprice FROM order_details 
        JOIN product ON product.product_id = order_details.product_id
        WHERE order_id = '$order_id'";
        $querysum = mysqli_query($conn, $sqlsum);
        $resultsum = mysqli_fetch_array($querysum);
        if ($resultsum) {
            $data['data'][$key]['sumprice'] = $resultsum['sumprice'];
            http_response_code(200);
        } else {
            $data['message'] = "ไม่มีข้อมูลการสั่งสินค้า";
            $data['quantity'] = 0;
            http_response_code(400);
        }
    }
    $data['message'] = "ดึงข้อมูลผู้จัดส่งสินค้าสำเร็จ";
    http_response_code(200);
} else {
    $data['message'] = "ไม่สามารถดึงข้อมูลผู้จัดส่งสินค้าได้";
    http_response_code(400);
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);
mysqli_close($conn);
