<?php
require_once('../database.php');
if ($_POST['pay_id'] && $_POST['order_id']) {
    $pay_id = $_POST['pay_id'];
    $order_id = $_POST['order_id'];
    $update_payment_order = "UPDATE `payment` SET `confirm` ='yes' WHERE  `pay_id`='$pay_id';";
    $update_payment_order .= "UPDATE `order` SET `paid` ='yes' WHERE `order_id`='$order_id';";
    if (mysqli_multi_query($conn, $update_payment_order)) {
        
        $data['message'] = "ยืนยันข้อมูลสำเร็จ";
        http_response_code(200);
    } else {
        $data['message'] = "ไม่สามารถยืนยันข้อมูลลูกค้าได้";
        http_response_code(400);
    }
} else {
    $data['message'] = "การส่งข้อมูลไม่ถูกต้อง";
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
