<?php
require_once('../database.php');
if ($_POST['order_id']) {
    $order_id = $_POST['order_id'];

    $sql = " SELECT SUM(CASE WHEN order_details.quantity > product.quantity THEN '1' ELSE '0' END ) AS check_quantity FROM `order_details` 
            JOIN product ON product.product_id = order_details.product_id 
            WHERE order_id = '$order_id'";
    $query = mysqli_query($conn, $sql);
    $result = mysqli_fetch_array($query, MYSQLI_ASSOC);
    if ($result['check_quantity'] > 0) {
        $data['message'] = "จำนวนสินค้าที่จะตัดไม่เพียงพอ";
        http_response_code(400);
    } else {
        $sql_balance = " SELECT (product.quantity - order_details.quantity) as balance,order_details.product_id FROM `order_details`
            JOIN product ON product.product_id = order_details.product_id 
            WHERE order_id = '$order_id'";
        $query_balance = mysqli_query($conn, $sql_balance);
        $result_balance = mysqli_fetch_all($query_balance, MYSQLI_ASSOC);
        foreach ($result_balance as $key => $item) {
            $product_id = $item['product_id'];
            $balance = $item['balance'];
            $update_product = "UPDATE `product` SET `quantity` ='$balance' WHERE `product_id`='$product_id';";
            mysqli_multi_query($conn, $update_product);
        }
        $update_order = "UPDATE `order` SET `delivery` ='yes' WHERE `order_id`='$order_id';";
        if (mysqli_multi_query($conn, $update_order)) {
            $data['message'] = "ยืนยันข้อมูลสำเร็จ";
            http_response_code(200);
        } else {
            $data['message'] = "ไม่สามารถยืนยันข้อมูลลูกค้าได้";
            http_response_code(400);
        }
    }
} else {
    $data['message'] = "การส่งข้อมูลไม่ถูกต้อง";
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
