<?php
require_once('../database.php');
if ($_POST['sup_id']) {
    $sup_id = $_POST['sup_id'];
    $sql = "SELECT * FROM `suppliers` WHERE sup_id = '$sup_id'";
    $query = mysqli_query($conn, $sql);
    while ($result = mysqli_fetch_assoc($query)) {
        $data['sup_id'] = $result["sup_id"];
        $data['sup_name'] = $result["sup_name"];
        $data['address'] = $result["address"];
        $data['phone'] = $result["phone"];
        $data['contact_name'] = $result["contact_name"];
        $data['website'] = $result["website"];
    }
    $data['message'] = "ดึงข้อมูลผู้จัดส่งสินค้าสำเร็จ";
    http_response_code(200);
} else {
    $data['message'] = "ไม่มีรหัสผู้จัดส่งสินค้า";
    http_response_code(400);
}
echo json_encode($data, JSON_UNESCAPED_UNICODE);
mysqli_close($conn);
