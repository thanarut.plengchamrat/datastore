<?php
require_once('../database.php');
$sql = "SELECT * FROM `suppliers`";

$query = mysqli_query($conn, $sql);
$result = mysqli_fetch_all($query, MYSQLI_ASSOC);

if ($result) {
    $data['data'] = $result;
    $data['message'] = "ดึงข้อมูลผู้จัดส่งสินค้าสำเร็จ";
    http_response_code(200);
} else {
    $data['message'] = "ไม่สามารถดึงข้อมูลผู้จัดส่งสินค้าได้";
    http_response_code(400);
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);
mysqli_close($conn);
