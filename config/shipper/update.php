<?php
require_once('../database.php');
if ($_POST['sup_id'] && $_POST['sup_name'] && $_POST['address'] && $_POST['phone'] && $_POST['contact_name']) {
    $sup_id = $_POST['sup_id'];
    $sup_name = $_POST['sup_name'];
    $address = $_POST['address'];
    $phone = $_POST['phone'];
    $contact_name = $_POST['contact_name'];
    $website = $_POST['website'];

    $update_suppliers = "UPDATE suppliers SET sup_name='$sup_name',address='$address',phone='$phone',contact_name='$contact_name',website='$website' WHERE sup_id='$sup_id'";
    if (mysqli_query($conn, $update_suppliers)) {
        $data['message'] = "เปลี่ยนแปลงข้อมูลผู้จัดส่งสินค้าสำเร็จ";
        http_response_code(200);
    } else {
        $data['message'] = "ไม่สามารถเปลี่ยนแปลงข้อมูลผู้จัดส่งสินค้าได้";
        http_response_code(400);
    }
} else {
    $data['message'] = "การส่งข้อมูลผู้จัดส่งสินค้าไม่ถูกต้อง";
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
