<?php
require_once('./../../database.php');
if (!file_exists('./../../../upload/slip')) {
    if (!mkdir('./../../../upload/slip', 0777, true)) {
        die('Failed to create folders...');
    }
}

$cust_id = $_GET['cust_id'];
$order_id = $_GET['order_id'];

$type_image = $_FILES['file']['type'];

if ($type_image === 'image/png') {
    $set_type = '.png';
} else if ($type_image === 'image/jpeg') {
    $set_type = '.jpg';
} else {
    $set_type = '.gif';
}
sleep(2);
$sql = "SELECT * FROM payment WHERE order_id = '$order_id' AND cust_id = '$cust_id'";
$query = mysqli_query($conn, $sql);
$result = mysqli_fetch_array($query, MYSQLI_ASSOC);
if ($result) {
    http_response_code(400);
    $data['message'] = "คุณแจ้งการโอนเงินของออเดอร์นี้ไปแล้ว รอตรวจสอบ";
} else {
    $newfilename = strtotime(DATE("YmdHis")) . $set_type;
    if (move_uploaded_file($_FILES['file']['tmp_name'], './../../../upload/slip/' . $newfilename)) {
        $path = 'upload/slip/' . $newfilename;
        $insert_images = "INSERT INTO payment (order_id,cust_id,date_tranfer,confirm,slip_path) 
    VALUES ('$order_id','$cust_id',NOW(),'no','$path')";
        if (mysqli_query($conn, $insert_images)) {
            http_response_code(200);
            $data['message'] = "แจ้งการโอนเงินเรียบร้อยแล้ว";
        } else {
            http_response_code(400);
            $data['message'] = "ไม่สามารถแจ้งการโอนเงินได้";
        }
    } else {
        http_response_code(400);
        $data['message'] = "ไม่สามารถแจ้งการโอนเงินได้";
    }
}
echo json_encode($data);
mysqli_close($conn);
