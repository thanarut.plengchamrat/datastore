<?php
require_once('./../../database.php');
if ($_POST['order_id']) {
    $order_id = $_POST['order_id'];
    $sql = "SELECT order_details.*,product.product_id,product.product_name,price,product.quantity as product_qty FROM order_details 
    JOIN product ON product.product_id = order_details.product_id
    WHERE order_details.order_id = '$order_id'";
    $query = mysqli_query($conn, $sql);
    $result = mysqli_fetch_all($query, MYSQLI_ASSOC);
    if ($result) {
        $data['data'] = $result;
        http_response_code(200);
    } else {
        $data['message'] = "ไม่มีข้อมูลรายการออเดอร์";
        $data['quantity'] = 0;
        http_response_code(400);
    }
} else {
    $data['message'] = "ไม่มีรายการออเดอร์ที่กำลังหา";
    $data['quantity'] = 0;
    http_response_code(400);
}

echo json_encode($data);
mysqli_close($conn);
