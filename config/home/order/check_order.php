<?php
require_once('./../../database.php');
if ($_POST['phone']) {
    $phone = $_POST['phone'];
    $sql = "SELECT * FROM customer WHERE phone = '$phone'";
    $query = mysqli_query($conn, $sql);
    $result = mysqli_fetch_array($query, MYSQLI_ASSOC);
    if ($result) {
        $cust_id = $result['cust_id'];
        $data['cust_id'] = $result['cust_id'];
        $sql_order = "SELECT `order`.`order_id`,SUM(product.price * order_details.quantity) as sum_order FROM `order` 
        JOIN order_details ON order_details.order_id = order.order_id
        JOIN product ON product.product_id = order_details.product_id
        WHERE `order`.`cust_id` = '$cust_id' AND `order`.`paid` = 'no'
        GROUP BY `order`.`order_id`";
        $query_order = mysqli_query($conn, $sql_order);
        $result_order  = mysqli_fetch_all($query_order, MYSQLI_ASSOC);
        if ($result_order) {
            $data['order'] = $result_order;
            http_response_code(200);
        } else {
            $data['message'] = "ไม่มีข้อมูลรายการออเดอร์ที่สั่ง";
            http_response_code(400);
        }
    } else {
        $data['message'] = "ไม่มีข้อมูลของคุณ";
        http_response_code(400);
    }
} else {
    $data['message'] = "ไม่มีข้อมูลที่ต้องการค้นหา";
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
