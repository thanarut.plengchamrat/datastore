<?php
require_once('./../../database.php');
session_start();
$sid = session_id();
if ($_POST['cust_id']) {
    $cust_id = $_POST['cust_id'];
    $insert_order = "INSERT INTO `order` (cust_id,order_date,paid,delivery)
        VALUES ('$cust_id',NOW(),'no','no')";
    if (mysqli_query($conn, $insert_order)) {
        $last_id = $conn->insert_id;
        $order_id = $last_id;
        $cart = "SELECT * FROM `cart` 
            WHERE session_id = '$sid'
            ";
        $query = mysqli_query($conn, $cart);
        $result = mysqli_fetch_all($query, MYSQLI_ASSOC);

        foreach ($result as $key => $item) {
            $product_id = $item['product_id'];
            $attribute = $item['attribute'];
            $quantity = $item['quantity'];
            $insert_order_details = "INSERT INTO order_details (order_id,product_id,attribute,quantity)
            VALUES ('$order_id','$product_id','$attribute','$quantity')";
            mysqli_query($conn, $insert_order_details);
        }
        $delete_cart = "DELETE FROM cart WHERE session_id = '$sid'";
        if (mysqli_query($conn, $delete_cart)) {
            $data['message'] = "เพิ่มข้อมูลใหม่สำเร็จ";
            $data['session'] = $sid.'psa'.$sid;
            http_response_code(200);
        } else {
            $data['message'] = "ไม่สามารถลบข้อมูลในรถเข็นได้ได้";
            http_response_code(400);
        }
    } else {
        $data['message'] = "$insert_order ";
        http_response_code(400);
    }
} else {
    $data['message'] = "การส่งข้อมูลไม่ถูกต้อง";
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
