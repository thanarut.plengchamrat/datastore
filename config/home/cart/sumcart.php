<?php
require_once('./../../database.php');
session_start();
$sid = session_id();
$sql = "SELECT SUM(quantity) as qty FROM cart WHERE session_id = '$sid'";
$query = mysqli_query($conn, $sql);
$result = mysqli_fetch_array($query);
// echo $result;
if ($result) {
    $data['quantity'] = $result['qty'];
    http_response_code(200);
} else {
    $data['message'] = "ไม่มีข้อมูลการสั่งสินค้า";
    $data['quantity'] = 0;
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
