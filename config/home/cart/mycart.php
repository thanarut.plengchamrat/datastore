<?php
require_once('./../../database.php');
session_start();
$sid = session_id();
$sql = "SELECT cart.*,product.product_id,product.product_name,price,product.quantity as product_qty FROM cart 
JOIN product ON product.product_id = cart.product_id
WHERE session_id = '$sid'";
$query = mysqli_query($conn, $sql);
$result = mysqli_fetch_all($query, MYSQLI_ASSOC);
if ($result) {
    $data['data'] = $result;
    http_response_code(200);
} else {
    $data['message'] = "ไม่มีข้อมูลการสั่งสินค้า";
    $data['quantity'] = 0;
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
