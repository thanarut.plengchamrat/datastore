<?php
require_once('./../../database.php');
if ($_POST['item_id'] && $_POST['quantity'] && $_POST['product_id']) {
    $item_id = $_POST['item_id'];
    $quantity = $_POST['quantity'];
    $product_id = $_POST['product_id'];

    $check_qty = "SELECT * FROM `product`
    WHERE product.product_id = '$product_id'";
    $query_qty = mysqli_query($conn, $check_qty);
    $result_qty = mysqli_fetch_array($query_qty);

    if ($quantity > $result_qty['quantity']) {
        $data['message'] = "จำนวนสินค้ามากกว่าที่มีอยู่";
        http_response_code(400);
        echo json_encode($data);
        exit();
    }

    $update_cart = "UPDATE cart SET quantity='$quantity' WHERE item_id='$item_id'";
    if (mysqli_query($conn, $update_cart)) {
        $data['message'] = "เปลี่ยนแปลงข้อมูลสำเร็จ";
        http_response_code(200);
    } else {
        $data['message'] = "ไม่สามารถเปลี่ยนแปลงข้อมูลได้";
        http_response_code(400);
    }
} else {
    $data['message'] = "การส่งข้อมูลไม่ถูกต้อง";
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
