<?php
require_once('./../../database.php');
if ($_POST['item_id']) {
    $item_id = $_POST['item_id'];
    $delete_cart = "DELETE FROM cart WHERE item_id = '$item_id'";
    if (mysqli_query($conn, $delete_cart)) {
        $data['message'] = "ลบข้อมูลสินค้าสำเร็จ";
        http_response_code(200);
    } else {
        $data['message'] = "ไม่สามารถลบข้อมูลสินค้าได้";
        http_response_code(400);
    }
} else {
    $data['message'] = "การส่งข้อมูลสินค้าไม่ถูกต้อง";
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
