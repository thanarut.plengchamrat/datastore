<?php
require_once('./../../database.php');
session_start();
$sid = session_id();
if ($_POST['product_id'] && $_POST['quantity']) {
    $product_id = $_POST['product_id'];
    $quantity = $_POST['quantity'];
    $sql = "SELECT * FROM `attributes`
    WHERE attributes.product_id = '$product_id'";
    $query = mysqli_query($conn, $sql);
    $result = mysqli_num_rows($query);
    $attrs = array();
    if ($result == 1) {
        if ($_POST['attr_name0']) {
            $attrs[0] = $_POST['attr_name0'];
        } else {
            $data['message'] = "โปรดเลือกข้อมูลให้ครบถ้วน";
            http_response_code(400);
            echo json_encode($data);
            exit();
        }
    } else if ($result == 2) {
        if ($_POST['attr_name0'] && $_POST['attr_name1']) {
            $attrs[0] = $_POST['attr_name0'];
            $attrs[1] = $_POST['attr_name1'];
        } else {
            $data['message'] = "โปรดเลือกข้อมูลให้ครบถ้วน";
            http_response_code(400);
            echo json_encode($data);
            exit();
        }
    } else if ($result == 3) {
        if ($_POST['attr_name0'] && $_POST['attr_name1'] && $_POST['attr_name3']) {
            $attrs[0] = $_POST['attr_name0'];
            $attrs[1] = $_POST['attr_name1'];
            $attrs[2] = $_POST['attr_name2'];
        } else {
            $data['message'] = "โปรดเลือกข้อมูลให้ครบถ้วน";
            http_response_code(400);
            echo json_encode($data);
            exit();
        }
    }
    mysqli_free_result($query);
    $att = implode(', ', $attrs);

    $check_qty = "SELECT * FROM `product`
    WHERE product.product_id = '$product_id'";

    $query_qty = mysqli_query($conn, $check_qty);
    $result_qty = mysqli_fetch_array($query_qty);

    if ($quantity > $result_qty['quantity']) {
        $data['message'] = "จำนวนสินค้ามากกว่าที่มีอยู่";
        http_response_code(400);
        echo json_encode($data);
        exit();
    }

    $check_cart = "SELECT * FROM `cart`
    WHERE cart.product_id = '$product_id' AND attribute = '$att' AND session_id = '$sid'";
    $query_cart = mysqli_query($conn, $check_cart);
    $result_cart = mysqli_num_rows($query_cart);

    if($result_cart > 0){
        $data['message'] = "มีสินค้านี้อยู่ในรถเข็นแล้ว";
        http_response_code(400);
        echo json_encode($data);
        exit();
    }

    mysqli_free_result($query_cart);


    $insert_cart = "REPLACE INTO cart (product_id,attribute,quantity,date_shop,session_id) 
    VALUES ('$product_id','$att','$quantity',NOW(),'$sid')";
    if (mysqli_query($conn, $insert_cart)) {
        $data['message'] = "หยิบใส่รถเข็นสำเร็จ";
        http_response_code(200);
    } else {
        $data['message'] = "ไม่สามารถหยิบใส่รถเข็นได้";
        http_response_code(400);
    }

} else {
    $data['message'] = "การส่งข้อมูลสินค้าไม่ถูกต้อง";
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
