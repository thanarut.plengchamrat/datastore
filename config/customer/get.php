<?php
require_once('../database.php');
$sql = "SELECT * FROM `customer`";

$query = mysqli_query($conn, $sql);
$result = mysqli_fetch_all($query, MYSQLI_ASSOC);

if ($result) {
    $data['data'] = $result;
    $data['message'] = "ดึงข้อมูลลูกค้าสำเร็จ";
    http_response_code(200);
} else {
    $data['message'] = "ไม่สามารถดูข้อมูลลูกค้าได้";
    http_response_code(400);
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);
mysqli_close($conn);
