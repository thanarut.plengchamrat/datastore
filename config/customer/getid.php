<?php
require_once('../database.php');
if ($_POST['cust_id']) {
    $cust_id = $_POST['cust_id'];
    $sql = "SELECT * FROM `customer` WHERE cust_id = '$cust_id'";
    $query = mysqli_query($conn, $sql);
    $result = mysqli_fetch_assoc($query);
    $data['data'] = $result;
    $data['message'] = "ดึงข้อมูลหมวดหมู่สำเร็จ";
    http_response_code(200);
} else {
    $data['message'] = "ไม่มีรหัสหมวดหมู่";
    http_response_code(400);
}
echo json_encode($data, JSON_UNESCAPED_UNICODE);
mysqli_close($conn);
