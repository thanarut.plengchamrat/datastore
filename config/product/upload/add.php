<?php
require_once('./../../database.php');
if (!file_exists('./../../../upload/product')) {
    if (!mkdir('./../../../upload/product', 0777, true)) {
        die('Failed to create folders...');
    }
}
$type_image = $_FILES['file']['type'];

if ($type_image === 'image/png') {
    $set_type = '.png';
} else if ($type_image === 'image/jpeg') {
    $set_type = '.jpg';
} else {
    $set_type = '.gif';
}
sleep(2);
$newfilename = strtotime(DATE("YmdHis")) . $set_type;
if (move_uploaded_file($_FILES['file']['tmp_name'], './../../../upload/product/' . $newfilename)) {
    $path = 'upload/product/' . $newfilename;
    $product_id = $_GET['product_id'];
    $insert_images = "INSERT INTO images (path,product_id) VALUES ('$path','$product_id')";
    if (mysqli_query($conn, $insert_images)) {
        http_response_code(200);
        $data['message'] = "เพิ่มรูปภาพสินค้าเรียบร้อยแล้ว";
    } else {
        http_response_code(400);
        $data['message'] = "ไม่สามารถเพิ่มรูปภาพสินค้าได้";
    }
} else {
    http_response_code(400);
    $data['message'] = "ไม่สามารถเพิ่มรูปภาพสินค้าได้";
}
echo json_encode($data);
mysqli_close($conn);
