<?php
require_once('./../../database.php');
if (!file_exists('./../../../upload/product')) {
    if (!mkdir('./../../../upload/product', 0777, true)) {
        die('Failed to create folders...');
    }
}
$product_id = $_GET['product_id'];
$type_image = $_FILES['file']['type'];

if ($type_image === 'image/png') {
    $set_type = '.png';
} else if ($type_image === 'image/jpeg') {
    $set_type = '.jpg';
} else {
    $set_type = '.gif';
}
sleep(2);
$newfilename = strtotime(DATE("YmdHis")) . $set_type;
if (move_uploaded_file($_FILES['file']['tmp_name'], './../../../upload/product/' . $newfilename)) {
    $path = 'upload/product/' . $newfilename;
    $update_images = "UPDATE images SET path ='$path' WHERE product_id='$product_id'";
    if (mysqli_query($conn, $update_images)) {
        http_response_code(200);
        $data['message'] = "เปลี่ยนรูปภาพสินค้าเรียบร้อยแล้ว";
        $data['path'] = $update_images;
    } else {
        http_response_code(400);
        $data['message'] = "ไม่สามารถเปลี่ยนรูปภาพสินค้าได้";
    }
} else {
    http_response_code(400);
    $data['message'] = "ไม่สามารถเปลี่ยนรูปภาพสินค้าได้";
}
echo json_encode($data);
mysqli_close($conn);