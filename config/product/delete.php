<?php
require_once('../database.php');
if ($_POST['product_id']) {
    $product_id = $_POST['product_id'];
    $delete_product = "DELETE FROM product WHERE product_id = '$product_id'";
    if (mysqli_query($conn, $delete_product)) {
        $data['message'] = "ลบข้อมูลสินค้าสำเร็จ";
        http_response_code(200);
    } else {
        $data['message'] = "ไม่สามารถลบข้อมูลสินค้าได้";
        http_response_code(400);
    }
} else {
    $data['message'] = "การส่งข้อมูลสินค้าไม่ถูกต้อง";
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
