<?php
require_once('./../../database.php');
if ($_POST['product_id']) {
    $product_id = $_POST['product_id'];

    $attr_name1 = $_POST['attr_name1'];
    $attr_name2 = $_POST['attr_name2'];
    $attr_name3 = $_POST['attr_name3'];

    $attr_value1 = $_POST['attr_value1'];
    $attr_value2 = $_POST['attr_value2'];
    $attr_value3 = $_POST['attr_value3'];

    if ($attr_name1 && $attr_value1) {
        $insert_attr1 = "INSERT INTO attributes (attr_name,attr_value,product_id)
        VALUES ('$attr_name1','$attr_value1','$product_id')";
        if (mysqli_query($conn, $insert_attr1)) {
            $status1 = 1;
        } else {
            $status1 = 0;
        }
    }
    if ($attr_name2 && $attr_value2) {
        $insert_attr2 = "INSERT INTO attributes (attr_name,attr_value,product_id)
        VALUES ('$attr_name2','$attr_value2','$product_id')";
        if (mysqli_query($conn, $insert_attr2)) {
            $status2 = 1;
        } else {
            $status2 = 0;
        }
    }
    if ($attr_name3 && $attr_value3) {
        $insert_attr3 = "INSERT INTO attributes (attr_name,attr_value,product_id)
        VALUES ('$attr_name3','$attr_value3','$product_id')";
        if (mysqli_query($conn, $insert_attr3)) {
            $status3 = 1;
        } else {
            $status3 = 0;
        }
    }

    if (($status1 == 1) || ($status2 == 1) || ($status3 == 1)) {
        $data['message'] = "เพิ่มข้อมูลสินค้า [คุณลักษณะ] สำเร็จ";
        http_response_code(200);
    } else {
        $data['message'] = "ไม่สามารถเพิ่มข้อมูลสินค้า [คุณลักษณะ]";
        http_response_code(400);
    }
} else {
    $data['message'] = "การส่งข้อมูลสินค้าไม่ถูกต้อง";
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
