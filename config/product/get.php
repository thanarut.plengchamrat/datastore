<?php
require_once('../database.php');
$sql = "SELECT product.*,suppliers.*,category.*,images.path FROM `product` 
LEFT JOIN images ON images.product_id = product.product_id
JOIN suppliers ON suppliers.sup_id = product.sup_id
JOIN category ON category.category_id = product.category_id
";

$query = mysqli_query($conn, $sql);
$result = mysqli_fetch_all($query, MYSQLI_ASSOC);
$data['data'] = $result;
foreach ($result as $key => $item) {
    $product_id = $item['product_id'];
    $attr = "SELECT * FROM `attributes` WHERE product_id = '$product_id'";
    $attr_query = mysqli_query($conn, $attr);
    $attr_result = mysqli_fetch_all($attr_query, MYSQLI_ASSOC);
    $data['data'][$key]['attr'] = $attr_result;
}
if ($result) {
    $data['message'] = "ดึงข้อมูลสินค้าสำเร็จ";
    http_response_code(200);
} else {
    $data['message'] = "ไม่สามารถดูข้อมูลสินค้าได้";
    http_response_code(400);
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);
mysqli_close($conn);
