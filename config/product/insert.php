<?php
require_once('../database.php');
if ($_POST['product_name'] && $_POST['product_detail'] && $_POST['sup_id'] && $_POST['price'] && $_POST['quantity'] && $_POST['category_id']) {
    $product_name = $_POST['product_name'];
    $sup_id = $_POST['sup_id'];
    $price = $_POST['price'];
    $quantity = $_POST['quantity'];
    $category_id = $_POST['category_id'];
    $product_detail = $_POST['product_detail'];
    $insert_product = "INSERT INTO product (product_name,sup_id,price,quantity,category_id,product_detail) VALUES ('$product_name','$sup_id','$price','$quantity','$category_id','$product_detail')";
    if (mysqli_query($conn, $insert_product)) {
        $last_id = $conn->insert_id;
        $data['product_id'] = $last_id;
        $data['message'] = "เพิ่มข้อมูลสินค้าสำเร็จ";
        http_response_code(200);
    } else {
        $data['message'] = "ไม่สามารถเพิ่มข้อมูลสินค้าได้";
        http_response_code(400);
    }
} else {
    $data['message'] = "การส่งข้อมูลสินค้าไม่ถูกต้อง";
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
