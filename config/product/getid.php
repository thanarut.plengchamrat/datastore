<?php
require_once('../database.php');
if ($_POST['product_id']) {
    $product_id = $_POST['product_id'];
    $sql = "SELECT product.*,images.path FROM `product` 
    LEFT JOIN images ON images.product_id = product.product_id
    WHERE product.product_id = '$product_id'";
    $query = mysqli_query($conn, $sql);
    $result = mysqli_fetch_assoc($query);
    $attr = "SELECT * FROM `attributes` WHERE product_id = '$product_id'";
    $attr_query = mysqli_query($conn, $attr);
    $attr_result = mysqli_fetch_all($attr_query, MYSQLI_ASSOC);
    $data['data'] = $result;
    $data['data']['attr'] = $attr_result;
    $data['message'] = "ดึงข้อมูลสินค้าสำเร็จ";
    http_response_code(200);
} else {
    $data['message'] = "ไม่มีรหัสสินค้า";
    http_response_code(400);
}
echo json_encode($data, JSON_UNESCAPED_UNICODE);
mysqli_close($conn);
