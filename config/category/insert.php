<?php
require_once('../database.php');
if ($_POST['category_name']) {
    $category_name = $_POST['category_name'];
    $insert_category = "INSERT INTO category (category_name)
        VALUES ('$category_name')";
    if (mysqli_query($conn, $insert_category)) {
        $data['message'] = "เพิ่มข้อมูลหมวดหมู่สำเร็จ";
        http_response_code(200);
    } else {
        $data['message'] = "ไม่สามารถเพิ่มข้อมูลหมวดหมู่ได้";
        http_response_code(400);
    }
} else {
    $data['message'] = "การส่งข้อมูลหมวดหมู่ไม่ถูกต้อง";
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
