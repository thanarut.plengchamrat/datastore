<?php
require_once('../database.php');
if ($_POST['category_id']) {
    $category_id = $_POST['category_id'];
    $delete_category = "DELETE FROM category WHERE category_id='$category_id'";
    if (mysqli_query($conn, $delete_category)) {
        $data['message'] = "ลบข้อมูลหมวดหมู่สำเร็จ";
        http_response_code(200);
    } else {
        $data['message'] = "ไม่สามารถลบข้อมูลหมวดหมู่ได้";
        http_response_code(400);
    }
} else {
    $data['message'] = "การส่งข้อมูลหมวดหมู่ไม่ถูกต้อง";
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
