<?php
require_once('../database.php');
if ($_POST['category_id']) {
    $category_id = $_POST['category_id'];
    $sql = "SELECT * FROM `category` WHERE category_id = '$category_id'";
    $query = mysqli_query($conn, $sql);
    while ($result = mysqli_fetch_assoc($query)) {
        $data['category_id'] = $result["category_id"];
        $data['category_name'] = $result["category_name"];
    }
    $data['message'] = "ดึงข้อมูลหมวดหมู่สำเร็จ";
    http_response_code(200);
} else {
    $data['message'] = "ไม่มีรหัสหมวดหมู่";
    http_response_code(400);
}
echo json_encode($data, JSON_UNESCAPED_UNICODE);
mysqli_close($conn);
