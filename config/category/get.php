<?php
require_once('../database.php');
$sql = "SELECT * FROM `category`";

$query = mysqli_query($conn, $sql);
$result = mysqli_fetch_all($query, MYSQLI_ASSOC);

if ($result) {
    $data['data'] = $result;
    $data['message'] = "ดึงข้อมูลหมวดหมู่สำเร็จ";
    http_response_code(200);
} else {
    $data['message'] = "ไม่สามารถดูข้อมูลหมวดหมู่ได้";
    http_response_code(400);
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);
mysqli_close($conn);
