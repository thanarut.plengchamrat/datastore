<?php
require_once('../database.php');
if ($_POST['category_id'] && $_POST['category_name']) {
    $category_id = $_POST['category_id'];
    $category_name = $_POST['category_name'];
    $update_category = "UPDATE category SET category_name='$category_name' WHERE category_id='$category_id'";
    if (mysqli_query($conn, $update_category)) {
        $data['message'] = "เปลี่ยนแปลงข้อมูลหมวดหมู่สำเร็จ";
        http_response_code(200);
    } else {
        $data['message'] = "ไม่สามารถเปลี่ยนแปลงข้อมูลหมวดหมู่ได้";
        http_response_code(400);
    }
} else {
    $data['message'] = "การส่งข้อมูลหมวดหมู่ไม่ถูกต้อง";
    http_response_code(400);
}
echo json_encode($data);
mysqli_close($conn);
