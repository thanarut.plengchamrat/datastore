<?php require('header.php'); ?>
<div class="page-content mb-5">
    <section id="multiple-addons">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">ประวัติการสั่งซื้อ</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-10 mb-2">
                                    <div class="input-group">
                                        <input type="text" id="search_phone" class="form-control" aria-label="ค้นหาโดยเบอร์โทรศัพท์" placeholder="ค้นหาโดยเบอร์โทรศัพท์">
                                    </div>
                                </div>
                                <div class="col-sm-2 mb-1">
                                    <a class="btn btn-info btn-block" onclick="check_order();">ค้นหา</a>
                                </div>
                            </div>
                            <div id="titlehistory" class="text-center mt-2 mb-2"></div>
                            <div id="show_history"></div>
                            <div class="row mt-2">
                                <div class="col-md-2 mb-2">
                                    <a href="<?= $base_url ?>home" style="width: 100%;" class="btn btn-primary">หน้าหลัก</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-dark mb-0">
                        <thead>
                            <tr>
                                <th class="text-center">ชื่อสินค้า</th>
                                <th class="text-center">คุณลักษณะ</th>
                                <th class="text-center">จำนวน</th>
                                <th class="text-center">ราคา</th>
                                <th class="text-center">รวม</th>
                            </tr>
                        </thead>
                        <tbody id="list_order">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function check_order() {
        var searchorder = $('#search_phone').val();
        if (searchorder) {
            var show_history = '';
            show_history += '<div class="table-responsive">';
            show_history += '<table class="table table-dark mb-0">';
            show_history += '<thead>';
            show_history += '<tr>';
            show_history += '<th class="text-center">รหัสการสั่งซื้อ</th>';
            show_history += '<th class="text-center">วันที่</th>';
            show_history += '<th class="text-center">สถานะการจ่ายเงิน</th>';
            show_history += '<th class="text-center">สถานะการจัดส่ง</th>';
            show_history += '<th class="text-center">ข้อมูล</th>';
            show_history += '</tr>';
            show_history += '</thead>';
            show_history += '<tbody id="loadhistory">';
            show_history += '</tbody>';
            show_history += '</table>';
            show_history += '</div>';
            $('#show_history').html(show_history);

            var loadhistory = '';
            $.ajax({
                method: "POST",
                url: "<?= $base_url ?>config/home/order/history",
                dataType: "json",
                data: {
                    phone: searchorder
                },
                async: false,
                success: function(data) {
                    var titlehistory = '';
                    titlehistory += '<h1>ประวัติการสั่งซื้อของ ' + data.data.firstname + ' ' + data.data.lastname + '</h1>';

                    $.each(data.order, function(key, val) {
                        loadhistory += '<tr>';
                        loadhistory += '<td class="text-center">' + val.order_id + '</td>';
                        loadhistory += '<td class="text-center">' + val.order_date + '</td>';
                        if (val.paid == 'no') {
                            loadhistory += '<td class="text-center text-danger">รอการตรวจสอบ</td>';
                        } else {
                            loadhistory += '<td class="text-center text-success">จ่ายเงินแล้ว</td>';
                        }

                        if (val.delivery == 'no' && val.paid == 'no') {
                            loadhistory += '<td class="text-center text-danger">รอการตรวจสอบ</td>';
                        } else if (val.delivery == 'no' && val.paid == 'yes') {
                            loadhistory += '<td class="text-center text-warning">รอการจัดส่ง</td>';
                        } else if (val.delivery == 'yes' && val.paid == 'yes') {
                            loadhistory += '<td class="text-center text-success">จัดส่งแล้ว</td>';
                        }

                        loadhistory += '<td><a class="btn btn-primary btn-block" onclick="list_order(' + val.order_id + ');">ข้อมูล</a></td>';
                        loadhistory += '</tr>';
                    })
                    
                    $('#titlehistory').html(titlehistory);
                    $('#loadhistory').html(loadhistory);

                },
                error: function(err) {
                    $.notify(err.responseJSON.message, 'error');
                }
            });
        } else {

        }
    }

    function list_order(order_id) {
        var list_order = '';
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/home/order/history_detail",
            dataType: "json",
            async: false,
            data: {
                order_id: order_id
            },
            success: function(data) {
                var sum_all = 0;
                $.each(data.data, function(key, val) {
                    console.log(data);
                    var sum = val.quantity * val.price;
                    sum_all = sum_all + sum;
                    list_order += '<tr>';
                    list_order += '<td>' + val.product_name + '</td>';
                    list_order += '<td class="text-center">' + val.attribute + '</td>';
                    list_order += '<td class="text-center">' + val.quantity + '</td>';
                    list_order += '<td class="text-center">' + new Intl.NumberFormat().format(val.price) + '</td>';
                    list_order += '<td class="text-center">' + new Intl.NumberFormat().format(sum) + '</td>';
                    list_order += '</tr>';
                })
                list_order += '<tr><td colspan="3" class="text-center"><b>รวมทั้งหมด</b></td><td class="text-center"><b>' + new Intl.NumberFormat().format(sum_all) + '</b></td><td></td></tr>'
                $('#list_order').html(list_order);
            },
            error: function(err) {
                list_order += '<tr><td colspan="5" class="text-center">ไม่มีข้อมูลในขณะนี้</td></tr>';
                $('#list_order').html(list_order);
            }
        });
        $('#exampleModal').modal('show');
    }
</script>
<?php require('footer.php'); ?>