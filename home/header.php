<?php
if ($_SERVER['HTTPS']) {
    $base_url = 'https://localhost/datastore/';
} else {
    $new_url = (empty($_SERVER['HTTPS']) ? 'https' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    header("location: " . $new_url);
    exit(0);
    // $base_url = 'http://localhost/datastore/';
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Store</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= $base_url ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= $base_url ?>assets/vendors/iconly/bold.css">
    <link rel="stylesheet" href="<?= $base_url ?>assets/vendors/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?= $base_url ?>assets/vendors/bootstrap-icons/bootstrap-icons.css">
    <link rel="stylesheet" href="<?= $base_url ?>assets/css/app.css">
    <link rel="stylesheet" href="<?= $base_url ?>assets/vendors/fontawesome/all.min.css">
    <link rel="shortcut icon" href="<?= $base_url ?>assets/images/favicon.svg" type="image/x-icon">

    <script src="<?= $base_url ?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?= $base_url ?>assets/js/jquery.min.js"></script>

</head>
<script>
    $(document).ready(function() {
        loadcategory();
        check_cart();
    });

    function loadcategory() {
        var text_category = '';
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/category/get",
            dataType: "json",
            async: false,
            success: function(data) {
                text_category += '<li class="submenu-item">';
                text_category += '<a href="<?= $base_url ?>home?product=all" class="sidebar-link">';
                text_category += '<span>ทั้งหมด</span>';
                text_category += '</a>';
                text_category += '</li>';
                $.each(data.data, function(key, val) {
                    text_category += '<li class="submenu-item">';
                    text_category += '<a href="<?= $base_url ?>home?product=' + val.category_id + '" class="sidebar-link">';
                    text_category += '<span>' + val.category_name + '</span>';
                    text_category += '</a>';
                    text_category += '</li>';
                })
                $('#category').html(text_category);
            },
            error: function(err) {
                $.notify(err.responseJSON.message, 'error');
            }
        });
    }

    function check_cart() {
        setTimeout(check_cart, 1000);
        var text_cart = '';
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/home/cart/sumcart",
            dataType: "json",
            async: false,
            success: function(data) {
                text_cart += '<a href="cart" class="sidebar-link">';
                text_cart += '<i class="bi bi-grid-fill"></i>';
                if(data.quantity){
                    text_cart += '<span>รถเข็นของฉัน (' + data.quantity + ')</span>';
                }else{
                    text_cart += '<span>รถเข็นของฉัน (0)</span>';
                }
                text_cart += '</a>';
                $('#cart').html(text_cart);
            },
            error: function(err) {
                text_cart += '<a href="cart" class="sidebar-link">';
                text_cart += '<i class="bi bi-grid-fill"></i>';
                text_cart += '<span>รถเข็นของฉัน (0)</span>';
                text_cart += '</a>';
                $('#cart').html(text_cart);
            }
        });
    }
</script>

<body>
    <div id="app">
        <div id="sidebar" class="active">
            <div class="sidebar-wrapper active">
                <div class="sidebar-header">
                    <div class="d-flex justify-content-between">
                        <div class="logo">
                            <a href="#">
                                <i class="bi bi-basket-fill"></i>
                                สินค้า Online
                            </a>
                        </div>
                        <div class="toggler">
                            <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                        </div>
                    </div>
                </div>
                <div class="sidebar-menu">
                    <ul class="menu">
                        <li class="sidebar-title">หมวดหมู่</li>
                        <li class="sidebar-item  has-sub">
                            <a href="#" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>สินค้า</span>
                            </a>
                            <ul class="submenu ">
                                <div id="category"></div>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a href="slip" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>แจ้งการโอนเงิน</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="history" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>ประวัติการสั่งซื้อ</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <div id="cart"></div>
                        </li>
                    </ul>

                </div>



                <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
            </div>
        </div>

        <div id="main">
            <header class="mb-3">
                <a href="#" class="burger-btn d-block d-xl-none">
                    <i class="bi bi-justify fs-3"></i>
                </a>
            </header>