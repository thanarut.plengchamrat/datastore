<?php require('header.php'); ?>
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>การสั่งซื้อเสร็จสมบูรณ์</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">รถเข็นของฉัน</li>
                        <li class="breadcrumb-item" aria-current="page">ข้อมูลของฉัน</li>
                        <li class="breadcrumb-item active"><b>เสร็จสมบูรณ์</b></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="page-content mb-5">
    <section id="multiple-addons">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body text-center">
                            <i style="font-size: 10em;" class="bi bi-check-circle-fill text-success"></i>
                            <div>
                                <a class="btn btn-link" href="<?= $base_url ?>home/history">ไปยังหน้าประวัติการสั่งซื้อ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
<script>
    $(document).ready(function() {
        check_cart_in();
    });

    function check_cart_in() {
        var text_cart = '';
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/home/cart/sumcart",
            dataType: "json",
            async: false,
            success: function(data) {
                if (data.quantity) {
                    setTimeout(function() {
                        window.location = ("<?= $base_url ?>home/cart")
                    }, 100);
                } else {
                   
                }
            },
            error: function(err) {

            }
        });
    }
</script>
<?php require('footer.php'); ?>