<?php require('header.php'); ?>
<div class="page-content mb-5">
    <section id="multiple-addons">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">แจ้งการโอนเงิน</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 mb-1">
                                    <div class="input-group">
                                        <input onkeyup="check_order_customer();" type="text" id="searchphone" class="form-control" aria-label="ค้นหาโดยเบอร์โทรศัพท์" placeholder="ค้นหาโดยเบอร์โทรศัพท์">
                                    </div>
                                </div>
                                <div class="col-sm-12 mb-1">
                                    <div class="input-group">
                                        <input type="hidden" id="cust_id" class="form-control" aria-label="รหัสลูกค้า" placeholder="รหัสลูกค้า">
                                    </div>
                                </div>
                                <div class="col-sm-12 mb-2">
                                    <!-- <div class="input-group"> -->
                                    <div id="select_option"></div>
                                    <!-- </div> -->
                                </div>
                                <div id="form_slip"></div>
                                <div class="row mt-2">
                                    <div class="col-md-2 mb-2">
                                        <a href="<?= $base_url ?>home" style="width: 100%;" class="btn btn-danger">ย้อนกลับ</a>
                                    </div>
                                    <div class="col-md-8">
                                    </div>
                                    <div class="col-md-2">
                                        <a onclick="confirm();" id="confirm" style="width: 100%; display:none;" class="btn btn-success">ยืนยันการโอน</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </section>
</div>
<script>
    $(document).ready(function() {

    });

    function check_order_customer() {
        var searchphone = $('#searchphone').val();
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/home/order/check_order",
            dataType: "json",
            data: {
                phone: searchphone
            },
            async: false,
            success: function(data) {
                var cust_id = data.cust_id;
                $('#cust_id').val(cust_id);
                var select_option = '';
                select_option += '<select class="choices form-select" id="slip_order_id">';
                select_option += ' <option value="">เลือกรายการสั่งซื้อแล้วยอดเงินที่ต้องการจ่าย</option>';
                $.each(data.order, function(key, val) {
                    select_option += ' <option value="' + val.order_id + '">รายการสั่งซื้อที่ ' + val.order_id + '| ยอดทั้งหมด : ' + new Intl.NumberFormat().format(val.sum_order) + '</option>';
                })
                select_option += '</select>';
                $('#select_option').html(select_option);
                var form_slip = '';
                form_slip += '<div class="row">';
                form_slip += '<div class="col-md-12 mb-2">';
                form_slip += '<div class="input-group">';
                form_slip += '<input type="file" id="image_uploads" class="form-control" aria-label="slip" placeholder="slip">';
                form_slip += '</div>';
                $('#form_slip').html(form_slip);

                document.getElementById("confirm").style.display = "block";
            },
            error: function(err) {

            }
        });
    }

    function confirm() {
        var cust_id = $('#cust_id').val();
        var slip_order_id = $('#slip_order_id').val();
        if (cust_id && slip_order_id) {
            bootbox.confirm({
                message: "คุณต้องการส่งสลิปการโอนเงินใช่หรือไม่",
                buttons: {
                    confirm: {
                        label: 'ยืนยัน',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'ยกเลิก',
                        className: 'btn-danger'
                    }
                },
                closeButton: false,
                callback: function(result_confirm) {
                    if (result_confirm == true) {
                        var image_uploads = $('#image_uploads').val();
                        var filelength = document.getElementById('image_uploads').files.length;
                        if (filelength > 0) {
                            var fileInput = document.getElementById('image_uploads').files[0];
                            var fileName = fileInput.name;
                            var fileSize = fileInput.size;
                            var fileType = fileInput.type;
                            var fileModifiedDate = fileInput.lastModifiedDate;
                            // ------------------------------------------------------------------
                            if (fileSize > 25000000) {
                                var file_max = "ไฟล์ " + fileName + " ของคุณมีขนาดเกิน 25 MB";
                                alert(file_max);
                                $('#image_uploads').val('');
                            } else {
                                if (fileType === 'image/jpeg' || fileType === 'image/jpg' || fileType === 'image/png' || fileType === 'image/gif') {
                                    var formData = new FormData();
                                    formData.append('file', fileInput);
                                    $.ajax({
                                        type: 'POST',
                                        url: '<?= $base_url ?>config/home/order/addslip?cust_id=' + cust_id + '&order_id=' + slip_order_id,
                                        dataType: "json",
                                        contentType: false,
                                        processData: false,
                                        data: formData,
                                        success: function(response) {
                                            $.notify(response.message, 'success');
                                            setTimeout(function() {
                                                window.location = ("<?= $base_url ?>home?product=all")
                                            }, 2000)
                                        },
                                        error: function(err) {
                                            $.notify(err.responseJSON.message, 'error');
                                        }
                                    });

                                }
                            }
                        } else {
                            $.notify("โปรดเพิ่มรูปภาพการโอนเงิน", 'error');
                        }
                    }
                }
            })
        } else {
            $.notify("โปรดใส่ข้อมูลให้ครบถ้วน", 'error');
        }
    }
</script>
<?php require('footer.php'); ?>