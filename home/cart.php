<?php require('header.php'); ?>
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>รถเข็นของฉัน</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active"><b>รถเข็นของฉัน</b></li>
                        <li class="breadcrumb-item" aria-current="page">ข้อมูลของฉัน</li>
                        <li class="breadcrumb-item" aria-current="page">เสร็จสมบูรณ์</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="page-content mb-5">
    <div class="table-responsive">
        <table class="table table-dark mb-0">
            <thead>
                <tr>
                    <th class="text-center">ชื่อสินค้า</th>
                    <th class="text-center">คุณลักษณะ</th>
                    <th class="text-center">จำนวน</th>
                    <th class="text-center">ราคา</th>
                    <th class="text-center">รวม</th>
                    <th class="text-center" width="20%" colspan="2">แก้ไข/ลบ</th>
                </tr>
            </thead>
            <tbody id="loadmycart">
            </tbody>
        </table>
    </div>
    <div class="row mt-4">
        <div class="col-md-2 mb-2">
            <a href="<?= $base_url ?>home" style="width: 100%;" class="btn btn-primary">หน้าหลัก</a>
        </div>
        <div class="col-md-8">
        </div>
        <div class="col-md-2">
            <a href="<?= $base_url ?>home/customer" style="width: 100%;" class="btn btn-success">ขั้นตอนถัดไป</a>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        loadmycart();
    });

    function loadmycart() {
        var loadmycart = '';
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/home/cart/mycart",
            dataType: "json",
            async: false,
            success: function(data) {
                var sum_all = 0;
                $.each(data.data, function(key, val) {
                    var sum = val.quantity * val.price;
                    sum_all = sum_all + sum;
                    loadmycart += '<tr>';
                    loadmycart += '<td>' + val.product_name + '</td>';
                    loadmycart += '<td class="text-center">' + val.attribute + '</td>';
                    loadmycart += '<td class="text-center" width ="100px"><input type="number" min="1" max="' + val.product_qty + '" class="form-control text-center" id="' + val.item_id + '" value="' + val.quantity + '"></td>';
                    loadmycart += '<td class="text-center">' + new Intl.NumberFormat().format(val.price) + '</td>';
                    loadmycart += '<td class="text-center">' + new Intl.NumberFormat().format(sum) + '</td>';
                    loadmycart += '<td width="10%"><button class="btn btn-primary btn-block" onclick="update_cart(' + val.item_id + ',' + val.product_id + ');">บันทึก</button></td><td width="10%"><button class="btn btn-danger btn-block" onclick="delcart(' + val.item_id + ')">ลบ</button></td>';
                    loadmycart += '</tr>';

                })
                loadmycart += '<tr><td colspan="4" class="text-center"><b>รวมทั้งหมด</b></td><td class="text-center"><b>' + new Intl.NumberFormat().format(sum_all) + '</b></td><td></td><td></td></tr>'
                $('#loadmycart').html(loadmycart);
            },
            error: function(err) {
                loadmycart += '<tr><td colspan="6" class="text-center">ไม่มีข้อมูลในขณะนี้</td></tr>';
                $('#loadmycart').html(loadmycart);
            }
        });
    }

    function update_cart(item_id, product_id) {
        if (item_id) {
            var quantity = $('#' + item_id).val();
            $.ajax({
                method: "POST",
                url: "<?= $base_url ?>config/home/cart/update",
                dataType: "json",
                data: {
                    item_id: item_id,
                    quantity: quantity,
                    product_id: product_id
                },
                async: false,
                success: function(data) {
                    loadmycart();
                    $.notify(data.message, 'success');
                },
                error: function(err) {
                    $.notify(err.responseJSON.message, 'error');
                }
            });
        } else {
            $.notify('ไม่เจอ ID ที่จะเปลี่ยน', 'error');
        }
    }

    function delcart(item_id) {
        if (item_id) {
            $.ajax({
                method: "POST",
                url: "<?= $base_url ?>config/home/cart/delete",
                dataType: "json",
                data: {
                    item_id: item_id
                },
                async: false,
                success: function(data) {
                    loadmycart();
                    $.notify(data.message, 'success');
                },
                error: function(err) {
                    $.notify(err.responseJSON.message, 'error');
                }
            });
        } else {
            $.notify('ไม่เจอ ID ที่จะลบ', 'error');
        }
    }
</script>
<?php require('footer.php'); ?>