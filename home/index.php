<?php require('header.php'); ?>
<style>
    .img_card {
        max-height: 150px;
    }

    .img_modal {
        max-width: 300px;
    }
</style>
<div class="page-heading">
    <h3>รายการสินค้า</h3>
</div>
<div class="page-content">
    <div id="loadproduct"></div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">รายละเอียดสินค้า</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div id="list_product"></div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        loadproduct();
    });

    function loadproduct() {
        var text_product = '';
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/home/product/get?product=<?= $_GET['product'] ?>",
            dataType: "json",
            async: false,
            success: function(data) {
                text_product += '<div class="row">';
                $.each(data.data, function(key, val) {
                    var trimmedString = val.product_detail.substr(0, 30);
                    if (val.product_detail.length > trimmedString.length) {
                        trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")))
                    }
                    text_product += '<div class="col-md-4"">';
                    text_product += '<div class="card" style="width: 18rem; height:20rem">';
                    text_product += '<img class="card-img-top img_card" src="<?= $base_url ?>' + val.path + '" alt="Card image cap">';
                    text_product += '<div class="card-body">';
                    text_product += ' <h5 class="card-title">' + val.product_name + '</h5>';
                    text_product += '<p class="card-text">' + trimmedString + '</p>';
                    text_product += '<a class="btn btn-primary" onclick="listproduct(' + val.product_id + ')">รายละเอียด</a>';
                    text_product += '</div>';
                    text_product += '</div>';
                    text_product += '</div>';
                })
                text_product += '</div>';

                $('#loadproduct').html(text_product);
            },
            error: function(err) {
                $.notify(err.responseJSON.message, 'error');
            }
        });
    }

    function listproduct(product_id) {
        var list_product = '';

        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/home/product/getid?product_id=" + product_id,
            dataType: "json",
            async: false,
            success: function(data) {
                $.each(data.data, function(key, val) {
                    if (val.quantity == 0) {
                        list_product += '<div class="modal-body">';
                        list_product += '<div class="row">';
                        list_product += '<div class="col-sm-12 text-center">';
                        list_product += '<h3 class="text-danger">สินค้าหมด</h3>';
                        list_product += '</div>';
                        list_product += '</div>';
                        list_product += '</div>';
                    } else {
                        list_product += '<div class="modal-body">';
                        list_product += '<div class="row">';
                        list_product += '<div class="col-sm-12 text-center mb-2">';
                        list_product += '<img class="card-img-top img_modal" src="<?= $base_url ?>' + val.path + '" alt="Card image cap">';
                        list_product += '</div>';

                        list_product += '<div class="col-sm-12 mb-1">';
                        list_product += '<p style="text-align: justify;text-justify: inter-word;">' + val.product_detail + '</p>';
                        list_product += '</div>';

                        list_product += '<div class="col-sm-12 mb-1">';
                        list_product += '<p><b>ราคา : </b>' + val.price + '</p>';
                        list_product += '</div>';

                        list_product += '<div class="col-sm-12 mb-1">';
                        list_product += '<p><b>หมวดหมู่ : </b>' + val.category_name + '</p>';
                        list_product += '</div>';

                        $.each(data.data[0].attr, function(keyattr, valattr) {
                            var str = valattr.attr_value;
                            var res = str.split(",");
                            list_product += '<div class="col-sm-3 mb-1">';
                            list_product += '<div class="input-group mb-3">';
                            list_product += '<select class="choices form-select" name="attr_name' + keyattr + '" id="attr_name' + keyattr + '">';
                            list_product += '<option value="">' + valattr.attr_name + '</option>';
                            for (var i = 0; i < res.length; i++) {
                                list_product += '<option value="' + valattr.attr_name + ':' + res[i] + '">' + res[i] + '</option>';
                            }
                            list_product += '</select>';
                            list_product += '</div>';
                            list_product += '</div>';
                        })
                        list_product += '<div class="col-sm-3 mb-1">';
                        list_product += '<div class="input-group mb-3">';
                        list_product += '<input type="number" min="1" max="' + val.quantity + '" id="quantity" class="form-control" name="quantity" placeholder="จำนวน">';
                        list_product += '</div>';
                        list_product += '</div>';

                        list_product += '</div>';
                        list_product += '</div>';

                        list_product += '<div class="modal-footer">';
                        list_product += '<button type="button" class="btn btn-primary" onclick="add_to_cart(' + val.product_id + ')">หยิบใส่รถเข็น</button>';
                        list_product += '</div>';
                    }
                })
            },
            error: function(err) {
                $.notify(err.responseJSON.message, 'error');
            }
        })
        $('#list_product').html(list_product);
        $('#exampleModal').modal('show');
    }

    function add_to_cart(product_id) {
        var attr_name0 = $('#attr_name0').val();
        var attr_name1 = $('#attr_name1').val();
        var attr_name2 = $('#attr_name2').val();
        var quantity = $('#quantity').val();

        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/home/cart/add",
            dataType: "json",
            async: false,
            data: {
                attr_name0: attr_name0,
                attr_name1: attr_name1,
                attr_name2: attr_name2,
                product_id: product_id,
                quantity: quantity
            },
            success: function(data) {
                $('#exampleModal').modal('hide');
                $.notify(data.message, 'success');
            },
            error: function(err) {
                $('#exampleModal').modal('hide');
                $.notify(err.responseJSON.message, 'error');
            }
        });
    }
</script>
<?php require('footer.php'); ?>