<?php require('header.php'); ?>
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>ข้อมูลของฉัน</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" aria-current="page">รถเข็นของฉัน</li>
                        <li class="breadcrumb-item active"><b>ข้อมูลของฉัน</b></li>
                        <li class="breadcrumb-item" aria-current="page">เสร็จสมบูรณ์</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="page-content mb-5">
    <section id="multiple-addons">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">กรอกข้อมูลของคุณ</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 mb-1">
                                    <div class="input-group">
                                        <input onkeyup="check_customer();" type="text" id="searchphone" class="form-control" aria-label="ค้นหาโดยเบอร์โทรศัพท์" placeholder="ค้นหาโดยเบอร์โทรศัพท์">
                                    </div>
                                </div>
                                <div class="col-sm-12 mb-1">
                                    <div class="input-group">
                                        <input type="hidden" id="cust_id" class="form-control" aria-label="รหัสลูกค้า" placeholder="รหัสลูกค้า">
                                        <input type="text" id="firstname" class="form-control" aria-label="ชื่อ" placeholder="ชื่อ">
                                    </div>
                                </div>
                                <div class="col-sm-12 mb-1">
                                    <div class="input-group">
                                        <input type="text" id="lastname" class="form-control" aria-label="นามสกุล" placeholder="นามสกุล">
                                    </div>
                                </div>
                                <div class="col-sm-12 mb-1">
                                    <div class="input-group">
                                        <input type="text" id="phone" class="form-control" aria-label="เบอร์โทรศัพท์" placeholder="เบอร์โทรศัพท์">
                                    </div>
                                </div>
                                <div class="col-sm-12 mb-1">
                                    <div class="input-group">
                                        <textarea class="form-control" id="address" rows="5" aria-label="ที่อยู่" placeholder="ที่อยู่"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-2 mb-2">
                                    <a href="<?= $base_url ?>home/cart" style="width: 100%;" class="btn btn-danger">ย้อนกลับ</a>
                                </div>
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-2">
                                    <a onclick="confirm();" style="width: 100%;" class="btn btn-success">ยืนยันการสั่งซื้อ</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
<script>
    $(document).ready(function() {
        check_cart_in();
    });

    function check_cart_in() {
        var text_cart = '';
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/home/cart/sumcart",
            dataType: "json",
            async: false,
            success: function(data) {

            },
            error: function(err) {
                setTimeout(function() {
                    window.location = ("<?= $base_url ?>home/cart")
                }, 100);
            }
        });
    }

    function check_customer() {
        var searchphone = $('#searchphone').val();
        $.ajax({
            method: "POST",
            url: "<?= $base_url ?>config/home/customer/get",
            dataType: "json",
            data: {
                phone: searchphone
            },
            async: false,
            success: function(data) {
                $.each(data, function(key, val) {
                    $('#cust_id').val(val.cust_id);
                    $('#firstname').val(val.firstname);
                    $('#lastname').val(val.lastname);
                    $('#phone').val(val.phone);
                    $('#address').val(val.address);
                })
            },
            error: function(err) {

            }
        });
    }

    function confirm() {
        var cust_id = $('#cust_id').val();
        var firstname = $('#firstname').val();
        var lastname = $('#lastname').val();
        var phone = $('#phone').val();
        var address = $('#address').val();

        if (firstname && lastname && phone && address) {

            bootbox.confirm({
                message: "ยืนยันการสั่งซื้อใช่หรือไม่",
                buttons: {
                    confirm: {
                        label: 'ยืนยัน',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'ยกเลิก',
                        className: 'btn-danger'
                    }
                },
                closeButton: false,
                callback: function(result_confirm) {
                    if (result_confirm == true) {
                        if (cust_id) { //ถ้าเคยมีข้อมูลแล้ว
                            $.ajax({
                                method: "POST",
                                url: "<?= $base_url ?>config/home/customer/update",
                                dataType: "json",
                                data: {
                                    firstname: firstname,
                                    lastname: lastname,
                                    phone: phone,
                                    address: address,
                                    cust_id: cust_id
                                },
                                async: false,
                                success: function(data) {
                                    $('#searchphone').val(phone);
                                    $.ajax({
                                        method: "POST",
                                        url: "<?= $base_url ?>config/home/order/add",
                                        dataType: "json",
                                        data: {
                                            cust_id: cust_id
                                        },
                                        async: false,
                                        success: function(result) {
                                            setTimeout(function() {
                                                window.location = ("<?= $base_url ?>home/success")
                                            }, 1000);
                                        },
                                        error: function(err) {
                                            $.notify(err.responseJSON.message, 'error');
                                        }
                                    });
                                },
                                error: function(err) {
                                    $.notify(err.responseJSON.message, 'error');
                                }
                            });
                        } else {
                            $.ajax({
                                method: "POST",
                                url: "<?= $base_url ?>config/home/customer/add",
                                dataType: "json",
                                data: {
                                    firstname: firstname,
                                    lastname: lastname,
                                    phone: phone,
                                    address: address
                                },
                                async: false,
                                success: function(data) {
                                    $.ajax({
                                        method: "POST",
                                        url: "<?= $base_url ?>config/home/order/add",
                                        dataType: "json",
                                        data: {
                                            cust_id: data.cust_id
                                        },
                                        async: false,
                                        success: function(result) {
                                            setTimeout(function() {
                                                window.location = ("<?= $base_url ?>home/success")
                                            }, 1000);
                                        },
                                        error: function(err) {
                                            $.notify(err.responseJSON.message, 'error');
                                        }
                                    });
                                },
                                error: function(err) {
                                    $.notify(err.responseJSON.message, 'error');
                                }
                            });
                        }
                    }
                }
            })
        } else {
            $.notify('โปรดกรอกข้อมูลให้ครบถ้วน', 'error');
        }

    }
</script>
<?php require('footer.php'); ?>