<footer>
    <div class="footer clearfix mb-0 text-muted">
        <div class="float-start">
            <p>2021 &copy; Datastore</p>
        </div>
        <div class="float-end">
            <p>Crafted with <span class="text-danger"></span> by Thanarut</p>
        </div>
    </div>
</footer>
</div>
</div>

<script src="<?= $base_url ?>assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="<?= $base_url ?>assets/js/main.js"></script>
<script src="<?= $base_url ?>assets/js/notify.js"></script>
<script src="<?= $base_url ?>assets/js/bootbox.min.js"></script>

</body>

</html>